# Tests
assert chiffre("hello", masque) =='+ 88*'
assert chiffre("nsi", masque) == '-6='

# Autres tests
message_1 = 10*"nsi"
assert chiffre(message_1, masque) == "-6=:69&!(=6,='?<2 #6':';+ =<6:"
assert chiffre('zz', masque) == '9?'
assert chiffre('9?', masque) == 'zz'
