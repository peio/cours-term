---
author: Mireille Coilhac, Gilles LASSUS et Fabrice NATIVEL
title: Cryptographie
---

# Sécurisation des communications

## Découverte


Lors de l'envoi d'un paquet, via le protocole HTTP, les routeurs successifs peuvent aisément accéder aux contenus envoyés. Dès lors il peuvent les lire, voire les modifier.

L'envoi d'informations sensibles, et le respect de la vie privée, ne sont donc pas compatibles avec ce protocole. La solution est de chiffrer les données, mais il n'est pas facile de le faire en respectant deux conditions :

!!! info "Mon info"

	* Le chiffrement doit être fiable, un tiers ne dois pas pouvoir décoder l'information.
	* le chiffrement doit être suffisamment rapide pour ne pas ralentir les flux de données.


!!! info "Le chiffrement"

	Nous allons étudier les principes du 

	* chiffrement symétrique 
	* chiffrement asymétrique

??? note "Vocabulaire déchiffrer ? décrypter ?"

	* **Chiffrer** : il s’agit de rendre un document illisible avec une clef de chiffrement
	* **Déchiffrer** : il s’agit de rendre lisible un document chiffré, en ayant connaissance de la clef de chiffrement
	* **Crypter** : cela n’existe pas
	* **Décrypter** : il s’agit de rendre lisible un document chiffré, sans avoir connaissance de la clef de chiffrement
	* **Cryptologie** : il s’agit de la science du secret

## I. Exemples de chiffrements symétriques

### 1. Le chiffrement de César

[Le chiffrement de César](https://codex.forge.apps.education.fr/exercices/code_cesar/){ .md-button target="_blank" rel="noopener" }

Nous pouvons appeler "clé" le décalage `decalage` de cet exercice.

???+ question

    La connaissance de cette clé permet-elle de déchiffrer un message chiffré ?

    ??? success "Solution"

        Pour déchiffrer, il suffit d'utiliser le même procédé avec la clé opposée, par exemple -4 si on a chiffré avec 4.

### 2. Le chiffrement avec xor

!!! info "xor"

	Une méthode, qui est encore utilisée de nos jours dans des algorithmes plus complexes, est d'utiliser une propriété de xor (ou exclusif).  

 	**a xor b** est vrai équivaut à seulement **a** est vrai ou seulement **b** est vrai, **mais pas les deux** (c'est pour cela que cela s'appelle le ou **exclusif**), et se note **xor**.  

	Voici la table de vérité de xor :

	| a | b | a xor b |
	|-:|-:|:-:|
	|T|T|F |
	|T|F|T |
	|F|T|T |
	|F|F|F |


!!! info "retrouver a si on connait b et a xor b"

	Si on connait **b** et **a xor b** on peut retrouver **a**.    
	En effet, regardons les 3 dernières colonnes du tableau ci-dessous.  
	La première et la dernière colonne du tableau sont identiques donc **b xor (a xor b)** est équivalent à **a **.  
	Si on connait **b** et **a xor b**, nous pouvons donc trouver **a** en faisant **b xor (a xor b)**.

	| a | b | a xor b |b xor (a xor b)
	|-:|-:|:-:|:-:|
	|T|T|F |T|
	|T|F|T |T|
	|F|T|T |F|
	|F|F|F |F|

!!! example "Exemple"

	Nous allons chiffrer "a" : (code 97) par xor avec la clé "n" (code 110)

	En binaire sur 8 bits : $97_{10} = (01100001)_2$ et $110_{10} = (01101110)_2$

	Effectuons  **$97_{10}$ xor $110_{10}$** bit à bit avec les encodages binaires sur 8 bits.

	<table>
	<tr>
    <td style="border:1px solid; font-weight:bold;"> </td>
    <td style="border:1px solid; font-weight:bold;">$97_{10}$</td>
    <td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
    <td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	</tr>
	<tr>
    <td style="border:1px solid; font-weight:bold;"> XOR </td>
    <td style="border:1px solid; font-weight:bold;">$110_{10}$</td>
    <td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
    <td style="border:1px solid; font-weight:bold;">1</td>
    <td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	</tr>
	<tr>
    <td style="border:1px solid; font-weight:bold;"> $97_{10}$ xor $110_{10}$ </td>
    <td style="border:1px solid; font-weight:bold;">=</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
    <td style="border:1px solid; font-weight:bold;">0</td>
    <td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	</tr>
	</table>


	Remarquons que $1111_2=15_{10}$

	Pour la transformation inverse :  


	<table>
	<tr>
    <td style="border:1px solid; font-weight:bold;"> </td>
    <td style="border:1px solid; font-weight:bold;">$97_{10}$ xor $110_{10}$</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
    <td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	</tr>
	<tr>
    <td style="border:1px solid; font-weight:bold;"> XOR </td>
    <td style="border:1px solid; font-weight:bold;">$110_{10}$</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
    <td style="border:1px solid; font-weight:bold;">1</td>
    <td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	</tr>
	<tr>
    <td style="border:1px solid; font-weight:bold;"> $(97_{10}$ xor $110_{10})$ xor $110_{10}$ </td>
    <td style="border:1px solid; font-weight:bold;">=</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
    <td style="border:1px solid; font-weight:bold;">1</td>
    <td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">0</td>
	<td style="border:1px solid; font-weight:bold;">1</td>
	</tr>
	</table>

	  
	La dernière ligne du deuxième tableau nous redonne bien l'encodage binaire de $97_{10}$.  
	Nous allons donc utiliser ces propriétés pour réaliser des chiffrements et déchiffrements.


!!! info "**xor** bit à bit en Python"

	xor** est effectué par `^` en Python.  
	⚠️ il faut écrire les nombres en décimal ! (ce qui nous arrangera pour le chiffrement par **xor**)


???+ question "Tester"  

    {{IDE('scripts/a_xor_n')}}

### 3. Une fonction de chiffrement avec xor

???+ question "Activité de masque jetable"

    On considère la variable suivante : `masque = "CETTEPHRASEESTVRAIMENTTRESTRESLONGUEMAISCESTFAITEXPRES"`

    * Créer une fonction `chiffre(message, masque)` qui chiffre `message` de type str en faisant un xor avec `masque` également de type str. On garantit que la longueur de `message ` est inférieure ou égale à celle de `masque` (54)
    * Cette fonction doit pouvoir **aussi** servir à déchiffrer le message chiffré.

    {{IDE('scripts/exo_xor')}}

    ??? tip "Astuce"

    	La fonction `ord` permet de renvoyer le code ASCII d'un caractère. La fonction `chr` fait l'opération inverse.
        ```python
        >>> ord('A')
        65
        >>> chr(65)
        'A'
        ```

## II. Le chiffrement symétrique

!!! info "La même clé"

    Dans un **chiffrement symétrique**, c'est **la même clé** qui va servir au chiffrement et au déchiffrement.

	![image](images/sym.png){: .center}

### 1. Qu'appelle-t-on une clé ?

!!! info "Qu'est-ce qu'une clé ?"

	La clé est un renseignement permettant de chiffrer ou déchiffrer un message. Cela peut être :

	- un nombre (dans un simple décalage des lettres de l'alphabet, comme [le chiffre de César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage))
	- une phrase (dans la méthode du [masque jetable](https://fr.wikipedia.org/wiki/Masque_jetable))
	- une image (imaginez un chiffrement où on effectue un XOR par les pixels d'une image, comme dans [cette énigme](https://github.com/glassus/nsi/blob/master/Premiere/DM/DM1/enonce.md))

!!! info "Chiffrement symétrique"

	Un chiffrement est dit symétrique lorsque la connaissance de la clé ayant servi au chiffrement permet de déchiffrer le message. Par exemple, Alice chiffre son message en décalant les lettres de 3 rangs vers la droite dans l'alphabet, Bob saura qu'il doit les décaler de 3 rangs vers la gauche pour retrouver le message initial.

### 2. Quel est l'avantage d'un chiffrement symétrique ?

!!! info "Avantages"

	Les chiffrements symétriques sont souvent rapides, consommant peu de ressources et donc adaptés au chiffrement de flux important d'informations.

	Comme nous le verrons, la sécurisation des données transitant par le protocole `https` est basée sur un chiffrement symétrique.

### 3. Quel est l'inconvénient d'un chiffrement symétrique ?

!!! info "Inconvénients"

	La clé ! Si Alice et Bob ont besoin d'utiliser un chiffrement pour se parler, comment peuvent-ils échanger leurs clés  puisque leur canal de transmission n'est pas sûr ?

	Le chiffrement symétrique impose qu'Alice et Bob aient pu se rencontrer physiquement au préalable pour convenir d'une clé secrète, ou bien qu'ils aient réussi à établir une connexion sécurisée pour s'échanger cette clé.

### 4.  Un chiffrement symétrique est-il un chiffrement de mauvaise qualité ?

Pas du tout ! S'il est associé naturellement à des chiffrements simples et faibles (comme le décalage de César), un chiffrement symétrique peut être très robuste... voire inviolable.

C'est le cas du masque jetable. Si le masque avec lequel on effectue le XOR sur le message est aussi long que le message, alors il est **impossible** de retrouver le message initial. Pourquoi ?

Imaginons qu'Alice veuille transmettre le message clair "LUNDI".
Elle le chiffre avec un masque jetable (que connait aussi Bob), et Bob reçoit donc "KHZOK". 
Si Marc a intercepté le message "KHZOK", *même s'il sait que la méthode de chiffrement utilisée est celle du masque jetable* (principe de Kerckhoffs), il n'a pas d'autre choix que de tester tous les masques de 5 lettres possibles.
Ce qui lui donne $26^5$ possibilités (plus de 11 millions) pour le masque, et par conséquent (propriété de bijectivité du XOR) $26^5$ possibilités pour le message «déchiffré»...

Cela signifie que Marc verra apparaître, dans sa tentative de déchiffrage, les mots "MARDI", "JEUDI", "JOUDI", "STYLO", "FSDJK", "LUNDI, "LUNDA"... 
Il n'a aucune possibilité de savoir où est le bon message original parmi toutes les propositions (on parle de *sécurité sémantique*).

!!! info "Principe de Kerckhoffs"

	la sécurité d'un système de chiffrement ne doit reposer que sur la sécurité de la clé, et non pas sur la connaissance de l'algorithme de chiffrement. Cet algorithme peut même être public (ce qui est pratiquement toujours le cas).

### 5. Quels sont les chiffrements symétriques modernes ?

L'algorithme de chiffrement symétrique le plus utilisé actuellement est le chiffrement [AES](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard), pour Advanced Encryption Standard.

- chiffrement par bloc de 128 bits, répartis dans une matrice de 16 octets (matrice carrée de taille 4).
- ces 128 bits sont transformés par des rotations, multiplications, transpositions, [...] de la matrice initiale, en faisant intervenir dans ces transformations une clé de 128, 192 ou 256 bits.
- pour l'AES-256 (avec une clé de 256 bits), l'attaque par force brute nécessiterait $2^{256}$ opérations, soit un nombre à 78 chiffres...
- il n'existe pas d'attaque connue efficace à ce jour. Les seules attaques sont des attaques sur des faiblesses d'implémentation, ou par [canal auxiliaire](https://fr.wikipedia.org/wiki/Attaque_par_canal_auxiliaire).


## III. Chiffrement asymétrique

!!! info "Présentation"

	Inventé par Whitfield Diffie et Martin Hellman en 1976, le chiffrement asymétrique vient résoudre l'inconvénient essentiel du chiffrement symétrique : le nécessaire partage d'un secret (la clé) avant l'établissement de la communication sécurisée.

	supposons qu'Alice envoie un message crypté à Bob. Sans la clef, Bob ne peut le lire. Et si on lui envoie la clef, un tiers malveillant risque de l'intercepter, rendant nos efforts de cryptage inefficaces !


### 1. Principe du chiffrement asymétrique

Alice veut envoyer un message chiffré à Bob.
Bob crée deux clés, une clé de chiffrement qu’il rend publique et une clé de déchiffrement qui reste privée (uniquement en possession de Bob).
Alice récupère la clé publique de Bob et peut chiffrer ses messages pour Bob. Seul Bob, qui possède la clé privée, peut les déchiffrer.

👩 ➡🧑
Donc si Alice veut envoyer un message à Bob, elle chiffre son message avec la clé publique de Bob.

👩↔🧑
Si les échanges se font dans les deux sens par chiffrement asymétrique, Alice crée elle aussi deux clés, une clé de chiffrement publique, que Bob utilise pour chiffrer les messages et une clé de déchiffrement privée qui reste en sa possession.

Dans le schéma suivant réalisé par Gilles LASSUS, Bob veut envoyer un message à Alice

![asymétrique](images/asym.png){: .center}


### 2. Prérequis d'arithmétique modulaire

### a. Les congruences

!!! example "Exemple"

	Il est 22h, quelle heure sera-t-il 8h plus tard ?

	Si vous avez répondu 6h (et pas 30h à la question précédente), vous venez de faire de l'*arithmétique modulaire*, en effet vous n'avez conservé que le reste dans la division euclidienne par 24:  
	$30 = 1 \times 24 + 6$ on écrira que $30 \equiv 6 [24]$ et on lira $30$ est égal à $6$ modulo $24$ ou $30$ est congru à $6$ modulo $24$

    Vérifions que $53 \equiv 5 [24]$. En effet $53 = 2 \times 24 + 5$

???+ question

    a. Compléter $103 \equiv \dots [24]$

    b. Compléter : $13 \equiv \dots [5]$

    c. Compléter : $42 \equiv \dots [7]$

    ??? success "Solution"

        a. $103 \equiv 7 [24]$ car $103 = 4 \times 24 + 7$

        b. $13 \equiv 3 [5]$ car $13 = 2 \times 5 + 3$

        c.  $42 \equiv 0 [7]$ car $42 = 6 \times 7 + 0$


### b. Nombres premiers et nombres premiers entre eux.

???+ question "Nombres premiers"

    Rappeler la définition d'un nombre premier. Les nombres suivants sont-ils premiers (justifier) : 12, 21, 29, 1 ?

    ??? success "Solution"

        Un nombre premier est un nombre qui a exactement deux diviseurs : 1 et lui même

        * 12 n'est pas premier car en plus de 1 et 12, il a aussi comme diviseur par exemple 2
        * 21 n'est pas premier car en plus de 1 et 21, il a aussi comme diviseur par exemple 3
        * 29 est premier, il n'a que 1 et 29 comme diviseurs
        * 1 n'est pas premier : il n'a qu'un seul diviseur : 1

!!! info "Nombres premiers entre eux"

	On dit que deux nombres sont premiers entre eux lorsque leur PGCD vaut 1.

!!! example "Exemple"

	* Par exemple 12 et 5 sont premiers entre eux
	* $33$ et $27$ ne sont pas premiers entre eux :  $33=3 \times 11$ et $27=3 ^3$. Leur PGCD est égal à 3. 

???+ question "Nombres premiers"

    Donner la liste des nombres premiers avec 12 qui sont inférieurs à 12.

    ??? success "Solution"

    	5, 7 et 11


### 3. Le chiffrement RSA

??? note pliée "Histoire"

	Lorsqu'en 1976 Diffie et Hellman (chercheurs à Stanford) présentent le concept de chiffrement asymétrique (souvent appelé _cryptographie à clés publiques_), ils en proposent uniquement un modèle théorique, n'ayant pas trouvé une réelle implémentation de leur protocole.

	Trois chercheurs du MIT (Boston), Ron Rivest, Adi Shamir et Len Adleman se penchent alors sur ce protocole, convaincus qu'il est en effet impossible d'en trouver une implémentation pratique. En 1977, au cours de leurs recherches, ils démontrent en fait l'inverse de ce qu'ils cherchaient : ils créent le premier protocole concret de chiffrement asymétrique : le chiffrement **RSA**.


	Au même moment à Londres, Clifford Cocks, (chercheur au très secret [GCHQ](https://fr.wikipedia.org/wiki/Government_Communications_Headquarters){. target="_blank"}) apprend que Rivest Shamir et Adleman viennent de découvrir ce que lui-même a découvert **3 ans auparavant** mais qui est resté classé Secret Défense.

	Il est le véritable inventeur du RSA... mais le reste du monde ne l'apprendra qu'en 1997 au moment de la déclassification de cette information. 


#### 2.3.1 Description

!!! info "Prérequis pour le RSA"

	Le chiffrement RSA est basé sur *l'arithmétique modulaire*. Faire des calculs *modulo* un entier $n$, c'est ne garder que le reste de la division euclidienne par $n$.

	Le fait que 15 soit égal à 1 modulo 7 (car $15=2 \times 7+1$) s'écrira $15 \equiv 1 [7]$.

	De même, $10 \equiv  3 [7]$, $25 \equiv 4 [7]$, $32 \equiv 2 [10]$, etc.


#### Étape 1

Alice choisit 2 grands nombres premiers $p$ et $q$. Dans la réalité ces nombres seront vraiment très grands (plus de 100 chiffres).  
Dans notre exemple, nous prendrons $p = 3$ et $q = 11$.

#### Étape 2

Alice multiplie ces deux nombres $p$ et $q$ et obtient ainsi un nombre $n$ appelé module de déchiffrement..

* 😊 Il est très facile pour Alice de calculer $n$ en connaissant $p$ et $q$.
* 😢 Il est extrêmement difficile pour Marc de faire le travail inverse : trouver $p$ et $q$ en connaissant $n$ prend un temps exponentiel avec la taille de $n$.  

C'est sur cette difficulté (appelée difficulté de *factorisation*) que repose la robustesse du système RSA.

#### Étape 3 : Alice crée sa clé publique

On note $\phi(n)$ le nombre $(p-1)(q-1)$. C'est l'indicatrice d'Euler.

Alice choisit un nombre $e$ appelé exposant de chiffrement, qui doit être premier avec $(p-1)(q-1)$.  

Dans notre exemple, $(p-1)(q-1) = 20$, Alice choisit donc $e = 3$. (mais elle aurait pu aussi choisir 7, 9, 13...).

Le couple **$(e, n)$** sera **la clé publique** d'Alice. Elle la diffuse à qui veut lui écrire.

Dans notre exemple, la clé publique d'Alice est $(3, 33)$.

#### Étape 4 : Alice calcule sa clé privée

Alice calcule maintenant sa clé privée : elle doit trouver un nombre *$d$* qui vérifie l'égalité $e \times d \equiv 1 [\phi(n)]$.

Dans notre exemple, comme $7 \times 3  \equiv 1 [20]$, ce nombre $d$ est égal à 7.

En pratique, il existe un algorithme simple (algorithme d'[Euclide étendu](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide_%C3%A9tendu)) pour trouver cette valeur $d$, appelée *inverse de e*.

Le couple $(d, n)$ sera **la clé privée** d'Alice. Elle ne la diffuse à personne.

Dans notre exemple, la clé privée d'Alice est $(7, 33)$.

#### Étape 5 : Bob envoie un message chiffré à Alice avec la clé publique d'Alice

Supposons que Bob veuille écrire à Alice pour lui envoyer le nombre 4. 
Il possède la clé publique d'Alice, qui est $(3, 33)$.

Il calcule donc $4^3$ modulo 33, qui vaut 31. C'est cette valeur 31 qu'il transmet à Alice.

$4^3 \equiv 31 [33]$

> Si Marc intercepte cette valeur 31, même en connaissant la clé publique d'Alice (3,33), il ne peut pas résoudre l'équation $x^3 \equiv 31 [33]$ de manière efficace.


#### Étape 6

Alice reçoit la valeur 31.  
Il lui suffit alors d'élever 31 à la puissance 7 (sa clé privée), et de calculer le reste modulo 33 :

$31^7 = 27512614111$

$27512614111 \equiv 4 [33]$

Elle récupère la valeur 4, qui est bien le message original de Bob.

![alice et bob](images/alice_bob.png){ width=50%; : .center }


> **Comment ça marche ?**
Grâce au [Petit Théorème de Fermat](https://fr.wikipedia.org/wiki/Petit_th%C3%A9or%C3%A8me_de_Fermat), on démontre (voir [ici](https://fr.wikipedia.org/wiki/Chiffrement_RSA)) assez facilement que $M^{ed} \equiv M [n]$.
Il faut remarquer que $M^{ed} = M^{de}$. On voit que les rôles de la clé publique et de la clé privée sont **symétriques** : un message chiffré avec la clé publique se déchiffrera en le chiffrant avec la clé privée, tout comme un message chiffré avec la clé privée se déchiffrera en le chiffrant avec la clé publique.

**Animation interactive**
voir [https://animations.interstices.info/interstices-rsa/rsa.html](https://animations.interstices.info/interstices-rsa/rsa.html)


### 4. Exercice : chiffrement RSA

???+ question "Exercice 1"

    Alice veut écrire à Bob.

	Soit le couple de nombre premiers $(p, q)$ avec $p=5$ et $q=13$.

	a. Calculer $n$ et $\phi(n)$.

    ??? success "Solution"

        * $n=5 \times 13 = 65$
		* $\phi(n)= 4 \times 12 = 48$


    b. Justifier que $(9, 65)$ ne peut pas être une clé publique.

	??? success "Solution"

		Pour que $(e, n)$ soit une clé publique, il faut que $e$ et $\phi(n)$ soient premiers entre eux. Or le PGCD de 9 et 48 est 3.  
		9 et 48 ne sont donc pas premiers entre eux.


    c. Vérifier que $(11, 65)$ est une clé publique. C'est la clé publique de Bob.

	??? success "Solution"

		Pour que $(e, n)$ soit une clé publique, il faut que $e$ et $\phi(n)$ soient premiers entre eux. Or le PGCD de 11 et 48 est 1.   
		11 et 48 sont donc premiers entre eux. On peut donc choisir $(11, 65)$ comme clé publique.

    d. Vérifier que 35 est un inverse de 11 modulo 48.

	??? success "Solution"

		$35 \times 11 =385$. Or $385=8 \times 48 + 1$  
		Donc $35 \times 11 = 1[48]$  
		On dit que 35 est un inverse de 11 modulo 48  

		Remarque : 

		```pycon
		>>> 385 % 48
		1
		```

    e. En déduire la clé privée de Bob.

	??? success "Solution"

		La clé privée de Bob est donc $(35, 65)$

	f. Chiffrer le nombre secret d'Alice 17 avec la clé publique de Bob. C'est ce nombre qu'Alice envoie à Bob.

	??? success "Solution"

		```pycon
		>>> (17**11) % 65
		23
		```
		17 sera chiffré par 23.  
		Alice envoie donc 23 à Bob.

	g. Déchiffrer le nombre reçu par Bob

	??? success "Solution"
	
		Bob doit déchiffrer 23. Pour cela il utilise sa clé privée qui est $(35, 65)$ :

		```pycon
		>>> (23**35) % 65
		17
		```
		Bob a bien déchiffré le nombre 17, qui était celui qu'Alice voulait lui envoyer.



!!! info "RSA, un système inviolable ?"

	Le chiffrement RSA a des défauts (notamment une grande consommation des ressources, due à la manipulation de très grands nombres).
	Mais le choix d'une clé publique de grande taille (actuellement 1024 ou 2048 bits) le rend pour l'instant inviolable. 

	Actuellement, il n'existe pas d'algorithme efficace pour factoriser un nombre ayant plusieurs centaines de chiffres.

	Deux évènements pourraient faire s'écrouler la sécurité du RSA :

	- la découverte d'un algorithme efficace de factorisation, capable de tourner sur les ordinateurs actuels. Cette annonce est régulièrement faite, et tout aussi régulièrement contredite par la communauté scientifique. (voir, le 05/03/2021,  [https://www.schneier.com/blog/archives/2021/03/no-rsa-is-not-broken.html](https://www.schneier.com/blog/archives/2021/03/no-rsa-is-not-broken.html))
	- l'avènement d'[ordinateurs quantiques](https://fr.wikipedia.org/wiki/Calculateur_quantique), dont la vitesse d'exécution permettrait une factorisation rapide. Il est à noter que l'algorithme de factorisation destiné à tourner sur un ordinateur quantique existe déjà : [l'algorithme de Schor](https://fr.wikipedia.org/wiki/Algorithme_de_Shor).


## IV. Attaque par l'homme du milieu

### 1. Exemple de scénario d'attaque par l'homme du milieu

![homme milieu](images/homme_milieu_1.PNG){ width=80%; : .center }

![homme milieu](images/homme_milieu_2.PNG){ width=80%; : .center }

![homme milieu](images/homme_milieu_3.PNG){ width=80%; : .center }

![homme milieu](images/homme_milieu_4.PNG){ width=80%; : .center }

![homme milieu](images/homme_milieu_5.PNG){ width=80%; : .center }


???+ question

    Ecrire la fonction `RSA` qui prend en paramètre une clé `cle`, et un entier `nbre`. Cette fonction renvoie le nombre chiffré ou déchiffré par RSA de `nbre` avec la clé `cle`.

	Vous pourrez vérifier toutes les valeurs indiquées dans le scénario ci-dessus.

	!!! example "Exemple"

		```pycon
		>>> RSA((11, 65), 17)
		23
		```

    {{IDE('scripts/exo_rsa')}}


!!! abstract "Résumé"

    Alice et Bob sont chacun persuadés d'utiliser la clé de l'autre, alors qu'ils utilisent en réalité tous les deux la clé de Jimmy.


!!! info "Man in the middle"

	Ce type d'attaque est appelé "Man in the middle". Elle peut être tentée contre RSA.

### 2. Certification

Pour se prémunir de ces attaques, une *autorité de certification* assure de l'identité d'un site afin d'éviter des attaques du type [*homme du milieu*](https://en.wikipedia.org/wiki/Man-in-the-middle_attack){target=_blank}, sans laquelle on pourrait se connecter à un site tiers en pensant qu'il s'agit par exemple de sa banque en ligne. Les requêtes `https` peuvent être observées à partir de la console de firefox. Pour cela :

!!! example "Exemple"

    Ecrire l'adresse : https://www.elysee.fr/ dans votre barre de navigation. Cliquer sur le cadenas, puis chercher le certificat.



## V. Le protocole HTTPS

!!! info "HTTPS"

    HTTPS : exemple d'utilisation conjointe d'un **chiffrement asymétrique** et d'un **chiffrement symétrique**.



### 3.1 Principe général

Aujourd'hui, plus de 90 % du trafic sur internet est chiffré : les données ne transitent plus en clair (protocole ```http```) mais de manière chiffrée (protocole ```https```), ce qui empêche la lecture de paquets éventuellements interceptés.

!!! info "HTTPS"

	Le protocole ```https``` est la réunion de deux protocoles :

	-  le protocole ```TLS``` (Transport Layer Security, qui a succédé au SSL) : ce protocole, basé sur du **chiffrement asymétrique**, va conduire à la génération d'une clé identique chez le client et chez le serveur.
	- le (bon vieux) protocole  ```http```, mais qui convoiera maintenant des données chiffrées avec la clé générée à l'étape précédente. Les données peuvent toujours être interceptées, mais sont illisibles. Le **chiffrement symétrique** utilisé est actuellement le chiffrement AES.

???+ note "Pourquoi ne pas utiliser que le chiffrement asymétrique, RSA par exemple ?"

	Le chiffrement RSA est très gourmand en ressources ! Le chiffrement/déchiffrement doit être rapide pour ne pas ralentir les communications ou l'exploitation des données.

	* Le chiffrement asymétrique est donc réservé à l'échange de clés (au début de la communication).
	* Le chiffrement symétrique, bien plus rapide, prend ensuite le relais pour l'ensemble de la communication.


![https](images/https.png){: .center}



### 3.2 (HP) Fonctionnement du TLS : explication du *handshake*

 Observons en détail le fonctionnement du protocole ```TLS```, dont le rôle est de générer de manière sécurisée une clé dont disposeront à la fois le client et le serveur, leur permettant ainsi d'appliquer un chiffrement symétrique à leurs échanges.

![tls](images/tls.png){: .center}


- **étape 1** : le «client Hello». Le client envoie sa version de TLS utilisée. 

- **étape 2** : le «server Hello». Le serveur répond en renvoyant son certificat prouvant son identité, ainsi que sa clé publique.

- **étape 3** : le client interroge l'autorité de certification pour valider le fait que le certificat est bien valide et que le serveur est bien celui qu'il prétend être. Cette vérification est faite grâce à un mécanisme de chiffrement asymétrique.

> La présentation du certificat à l'autorité de certification peut se représenter comme le scan d'une pièce d'identité dans un aéroport. L'autorité de certification est alors l'État (dont la base de données est interrogée par un logiciel) qui valide que la pièce d'identité est bien un document officiel.

- **étape 4** : une fois vérifiée l'authenticité du serveur et que son certificat est valide, le client calcule ce qui sera la future clé de chiffrement symétrique (appelée «clé AES» dans l'infographie). Cette clé est chiffrée avec la clé publique du server (transmise à l'étape 1), ce qui assure la sécurité de son transfert. Le serveur déchiffre cette clé grâce à sa clé privée, et dispose ainsi lui aussi de la clé. 

Le transmission par protocole ```http``` de données chiffrées au préalable avec la clé AES peut commencer. 

>Remarque : en réalité, ce n'est pas la clé AES qui est transmise à l'étape 4, mais un nombre choisi par le client, qui permettra, avec deux autres nombres choisis par le client (étape 1) et le serveur (étape 2) de reconstituer la clé AES, qui sera donc identique côté client et côté serveur.

## VI. QCM

???+ question "Question 1"

    === "Cocher la ou les affirmations correctes"

        - [ ] Dans un chiffrement symétrique, une seule clé est partagée
        - [ ] Dans un chiffrement asymétrique, deux clés sont partagées
        - [ ] Une clé de chiffrement doit toujours rester secrète
        - [ ] Le chiffrement RSA est symétrique

    === "Solution"

        - :white_check_mark: Dans un chiffrement symétrique, une seule clé est partagée
        - :x: Seule la clé publique est partagée
        - :x: Dans un chiffrement asymétrique, la clé de chiffrement est publique
        - :x: ~~Le chiffrement RSA est symétrique~~ 

???+ question "Question 2"

	Bob veut envoyer un message chiffré à Alice

    === "Cocher la ou les affirmations correctes"

        - [ ] Alice a besoin de la clé privée de Bob
        - [ ] Alice a besoin de la clé publique de Bob
        - [ ] Bob a besoin de la clé privée d'Alice
        - [ ] Bob a besoin de la clé publique d'Alice

    === "Solution"

        - :x: ~~Alice a besoin de la clé privée de Bob~~
        - :x: ~~Alice a besoin de la clé publique de Bob~~
        - :x: ~~Bob a besoin de la clé privée d'Alice~~
        - :white_check_mark: Bob a besoin de la clé publique d'Alice 

???+ question "Question 3"

	La protocole HTTPS

    === "Cocher la ou les affirmations correctes"

        - [ ] permet au client d'authentifier l'identité d'un site web auquel il souhaite accéder
        - [ ] garantit la confidentialité des données
        - [ ] garantit l'intégrité des données
        - [ ] utilise un chiffrement asymétrique pour la communication.

    === "Solution"

        - :white_check_mark: Il fournit un certificat d'authentification
        - :white_check_mark: garantit la confidentialité des données
        - :white_check_mark: Les données ne peuvent pa être modifiées entre le client et le serveur
        - :x: Le chiffrement asymétrique n'est utilisé que pour échanger une clé privée, qui sera ensuite utilisée pour chiffrer le message par chiffrement symétrique, bien plus rapide. 



