# --------- PYODIDE:env --------- #
def fabrique_liste(nb):
    lst = [k for k in range(nb)]
    return lst

def fabrique_dict(nb):
    dct = {k:k for k in range(nb)}
    return dct
# --------- PYODIDE:code --------- #
import time

def mesures(nb):
    tps_total = 0
    for _ in range(10):
        t0 = time.time()
        lst = fabrique_liste(nb)
        delta_t = time.time() - t0
        tps_total += delta_t
    tps_moyen_lst = tps_total / 10

    tps_total = 0
    for _ in range(10):
        t0 = time.time()
        d = fabrique_dict(nb)
        delta_t = time.time() - t0
        tps_total += delta_t
    tps_moyen_d = tps_total / 10

    print(f"temps pour liste de taille {nb}           : {tps_moyen_lst}")
    print(f"temps pour un dictionnaire de taille {nb} : {tps_moyen_d}")