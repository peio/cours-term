
# --------- PYODIDE:code --------- #

def fabrique_dict(nb):
    ...

# --------- PYODIDE:tests --------- #

assert fabrique_dict(5) == {0:0, 1:1, 2:2, 3:3, 4:4}
assert fabrique_dict(10) == {0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:7, 8:8, 9:9}
# --------- PYODIDE:corr --------- #

def fabrique_dict(nb):
    dico = {k:k for k in range(nb)}
    return dico


# --------- PYODIDE:secrets --------- #
dico = {k:k for k in range(1000)}
assert fabrique_dict(1000) == dico

