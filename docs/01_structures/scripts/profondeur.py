def parcours_en_profondeur(graphe, sommet):
    """
    Parcours d'un graphe en profondeur
    :param graphe: une liste d'adajence du graphe étudié (un dictionnaire)
    :param sommet: le sommet de départ du graphe étudié
    :return: la liste des sommets du graphe

    >>> parcours_en_profondeur({"A":("B","D","E"),"B":("A","C"),"C":("B","D"),"D":("A","C","E"),
    "E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),"H":("G")},"A")
    ['A', 'E', 'G', 'H', 'F', 'D', 'C', 'B']
    """
    if not(sommet in graphe.keys()):
        return None
    pile = ...
    liste_sommets = []
    ...




    return liste_sommets

#Tests
assert parcours_en_profondeur({"A":("B","D","E"),"B":("A","C"),"C":("B","D"),
"D":("A","C","E"),"E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),
"H":("G")},"A") == ['A', 'E', 'G', 'H', 'F', 'D', 'C', 'B']

