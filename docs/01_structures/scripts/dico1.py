
# --------- PYODIDE:code --------- #

def fabrique_liste(nb):
    ...

# --------- PYODIDE:tests --------- #

assert fabrique_liste(5) == [0, 1, 2, 3, 4]
assert fabrique_liste(10) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# --------- PYODIDE:corr --------- #

def fabrique_liste(nb):
    lst = [k for k in range(nb)]
    return lst


# --------- PYODIDE:secrets --------- #
tableau = [k for k in range(1000)]
assert fabrique_liste(1000) == tableau

