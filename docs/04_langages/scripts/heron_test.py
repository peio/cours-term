def racine(a) : 
    '''Méthode du Héron
    >>> racine(1)
    1
    >>> racine(4)
    2
    '''
    assert a >= 0
    x = 1
    for i in range(10):
          x = (x + a / x) / 2 
    assert x > 0
    return x