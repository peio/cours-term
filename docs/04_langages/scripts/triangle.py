# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

# --- PYODIDE:code --- #
from turtle import *

for i in range(3):
    forward(200)
    left(120) 

# --- PYODIDE:post --- #
done()
document.getElementById("cible_1").innerHTML = svg() 