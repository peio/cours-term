def racine(a) : 
    '''Méthode du Héron'''
    assert a >= 0
    x = 1
    for i in range(10):
          x = (x + a / x) / 2 
    assert x > 0
    return x