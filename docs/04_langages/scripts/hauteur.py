def hauteur(arbre):
    ...

# Tests
assert hauteur(((None, 6, None), 15, None)) == 2
assert hauteur((None, 6, None)) == 1
assert hauteur(None) == 0
