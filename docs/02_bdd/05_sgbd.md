---
author: Gilles Lassus, Pierre Marquestaut
title: Les SGBD
---

## Généralités

!!! note "Rappels"
    Les considérations sur le modèle relationnel du cours précédent traitaient plutôt de la structure mathématique des données. 

    Il s'agissait de déterminer la meilleure structure pour représenter les données et les relations qui les lient. 

    Il convient maintenant d'aborder la partie logicielle : les SGBD (Systèmes de Gestion de Bases de Données).

!!! abstract "Les SGBD"
    Les SGBD jouent le rôle d'interface entre l'être humain et la base de données.
    Par l'intermédiaire de **requêtes**, l'utilisateur va consulter ou modifier la base de données. Le SGBD est garant de l'intégrité de cette base, et prévient notamment que les modifications (souvent appelées **transactions**) ne soient pas préjudiciables à la base de données.

    Le langage utilisé pour communiquer avec le SGBD est le langage **SQL**, pour Structured  Query Langage (pour *langage de requêtes structurées*).

!!! info "Modèle relationnelle"

    Les SGBD les plus utilisés sont basés sur le modèle relationnel. Parmi eux, citons Oracle, MySQL, Microsoft SQL Server, PostgreSQL, Microsoft Access, SQLite, MariaDB...

    Mais de plus en plus de SGBD **non-relationnels** sont utilisés, spécialement adaptés à des données plus diverses et moins structurées. On les retrouve sous l'appelation **NoSQL**  (pour *Not only SQL*). Citons parmi eux MongoDB, Cassandra (Facebook), BigTable (Google)...

    ![image](images/stats.png){: .center}

!!! abstract "Client-Serveur"

    La quasi-totalité de ces SGBD fonctionnent avec un modèle client-serveur. 
    ![image](images/client-serveur.png){: .center}

## Le SGBD SQLite


Nous allons travailler principalement avec le module SQLite qui peut s'utiliser directement sans démarrer un serveur : la base de données est entièrement représentée dans le logiciel utilisant SQLite (dans notre cas, DB Browser for SQLite).  
Sa simplicité d'utilisation en fera notre choix pour illustrer cette présentation du langage SQL. 

!!! exercise "Premier programme"
    Après avoir importé le module `sqlite`, nous devons réaliser deux actions pour pouvoir commencer à utiliser notre base :

    * ouvrir le fichier de base de données
    * créer un curseur

    ```python title=""
    bdd = sqlite3.connect("albums3.db")
    curseur = bdd.cursor()
    ```

    Le *curseur* est un objet python offrant des méthodes pour exécuter des requêtes et récupérer le ou les résultats de ces requêtes.

    {{ IDE('scripts/sgbd1') }}

    *albums3.db* est le nom du fichier contenant la base de donnéees SQLite que nous allons exploiter. 

    * Si le fichier existe, la base de données est chargée en mémoire.
    * Si le fichier n'existe pas, une nouvelle base de données sera créée.

!!! exercise "Exécuter des requêtes de sélection"


    Reste ensuite à exécuter notre première requête. Pour cela, nous utiliserons la méthode **execute()** du curseur, la requête étant une chaîne de caractères passée en paramètre.

    ```python title=""
    requete = "SELECT * FROM Albums;"
    curseur.execute(requete)
    ```
    Pour visualiser le résultat de notre requête, nous utiliserons encore notre curseur. Deux méthodes permettent principalement de le faire :

    - `fetchone()` pour récupérer un résultat sous forme d'un *tuple* puis avancer le curseur d'un cran
    - `fetchall()` pour récupérer d'un coup tous les résultats sous la forme d'un *tableau de tuples*. 

    Le script ci-dessous permet donc de charger l'ensemble des enregistrements dans la variable `resultat` :

    {{ IDE('scripts/sgbd2') }}

!!! exercise  "Construire des requêtes à partir de variables python"

    Nous allons dans l'exemple suivant écrire une fonction `album()` : 

    - qui prend en paramètre un curseur et un classement d'album
    - qui renvoie le titre de l'album

    Si le classement de l'album ne figure pas dans la table *Albums*, la fonction renverra `None`.

    ```python title=""
    >>> album(curseur,1)
    "Sgt. Pepper's Lonely Hearts Club Band"
    >>> album(curseur, 2038)
    None
    ```

    {{ IDE('scripts/sgbd3') }}



!!! exercise  "A vous de jouer"

    La table `Artistes` à deux champs : `nom` et `id_artiste`.

    Ecrivez une fonction `artiste()`
    - qui prend en paramètre un curseur et un ID d'artiste (identifiant d'artiste)
    - qui renvoie le nom de l'artiste corespondant

    Si le nom de l'ID d'artiste ne figure pas dans la table *Artistes*, la fonction renverra `None`.

    {{ IDE('scripts/sgbd4') }}
