---
author: Jason Lapeyronnie, Gilles Lassus
title: Le modèle relationnel
---
# Le modèle relationnel


## 0. De la nécessité d'organiser le stockage de ses données

Lorsqu'une grande quantité de données doit être gérée, il faut savoir distinguer deux choses :

- la structure qui va décrire l'ensemble de ces données, notamment les liens entre elles, les hiérarchies éventuelles,...
- le moyen logiciel qui va aider à stocker et gérer ces données.

Pour gérer ces données, je peux : les écrire à la main dans mon agenda, créer un feuille de tableur avec Excel ou LibreOffice, utiliser une liste dans un IDE Python,...
Chaque méthode aura ses avantages et ses inconvénients.  


- Si le nombre de données à stocker devient très grand, est-ce que ma solution choisie pourra les gérer ?  (on peut par exemple méditer sur le cas du Royaume-Uni dont le comptage des patients positifs au Covid est devenu faux car il a dépassé les limites de leur [feuille Excel](https://www.numerama.com/politique/653217-16-000-anglais-malades-du-covid-ont-ete-oublies-a-cause-dune-feuille-excel-trop-pleine.html){. target="_blank"})

- Est-ce que d'autres personnes que moi sont susceptibles de consulter ou modifier ces données, éventuellement en même temps que moi ?


- Si une donnée se retrouve à plusieurs endroits dans mes données, devrais-je aller modifier cette donnée partout où elle se trouve ou bien une seule fois ?

L'étude des Bases de Données tente d'apporter des réponses à toutes ces questions.

## 1. Le modèle relationnel


!!! info "Bases de données relationnelles"
    ![codd](images/codd.jpg){: .center}
    Théorisé en 1970 par le Britannique Edgard J. Codd, 
    le modèle relationnel est à ce jour le modèle de base de données le plus utilisé, même si l'ère actuelle du Big Data tend à mettre en avant d'autres modèles non relationnels.

!!! info "Définition"

	Une base de données est un ensemble structuré d'informations.

	* Dans le langage courant, elle peut désigner n'importe quelle source importante d'informations (dictionnaires, encyclopédies, etc.)
	* En informatique, il s'agit d'informations stockées sous forme de fichiers et organisées de façon à être facilement manipulées.


Un modèle relationnel est donc basé sur des... **relations**.

## 2. Relations

Prenons l'exemple d'une bibliothèque dont la base de données possède une table «livres» :
![](images/rel_livres.png)

!!! question "Rappel de vocabulaire"

    Ci-dessous se trouvent des rappels sur le vocabulaire des données structurées.

    <iframe id="bdd"
        title="interblocage"
        width="100%"
        height="270"
        src="{{ page.canonical_url }}../activites/vocabulaire.html">
    </iframe>


!!! abstract "Vocabulaire"

    Dans le modèle relationnel, les données sont stockées dans des tables, également appelés **relations**

    - la première ligne de ce tableau constitue l'**entête** de la relation. Les éléments qui constituent l'entête s'appellent les **attributs** (ou **champs** ou encore **colonnes** ) de la relation.
    - le **domaine** de chaque attribut désigne l'ensemble des valeurs qui peuvent être prises par cet attribut.
    - chaque ligne de cette relation est appelé **enregistrement** ou **n-uplet**.


!!! note "Domaine"
    Le domaine est l'ensemble des valeurs possibles que peut prendre un champ.

    Le domaine est caractérisé notamment par son type.

!!! example "Exemple"

    * le domaine de l'attribut "id" correspond à l'ensemble des entiers (noté INT) : la colonne "id" devra obligatoirement contenir des entiers.
    * le domaine de l'attribut "titre" correspond à l'ensemble des chaînes de caractères (noté TEXT).
    * le domaine de l'attribut "note" correspond à l'ensemble des entiers positifs compris en 0 et 20.




## 3. Clé Primaire

!!! note "Clé primaire :heart:"
    Une clé primaire est un attribut (ou une réunion d'attributs) **dont la connaissance suffit à identifier avec certitude un unique enregistrement**.

Par exemple, la clé primaire de la relation des personnes nées en France pourrait être leur [numéro de Sécurité Sociale](https://fr.wikipedia.org/wiki/Num%C3%A9ro_de_s%C3%A9curit%C3%A9_sociale_en_France){:target="_blank"}.

???+ question "Exercice"

    === "Dans notre relation précédente, quel attribut peut être une clé primaire  ? (Cocher la ou les réponses correctes)"
        
        - [ ] Titre
        - [ ] Auteur
        - [ ] Éditeur
        - [ ] ISBN (International Standard Book Number)
        - [ ] Code

    === "Solution"
        
        - :white_check_mark: Titre  : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux livres ayant le même titre.
        - :white_check_mark: Auteur : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux livres ayant le même auteur.
        - :x: Éditeur : cet attribut ne peut pas jouer le rôle de clé primaire. En effet, la donnée de l'attribut «Actes Sud» renvoie vers 4 livres différents.
        - :white_check_mark: ISBN : cet attribut est un numéro unique spécifique à chaque livre : il peut jouer le rôle de clé primaire.
        - :white_check_mark: Code : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux livres ayant le même code.

!!! tips "Alors, quelle clé primaire choisir ?"

    Il faut pour cela réfléchir à ce que deviendrait notre table si elle contenait 1000 livres au lieu de 10. Il est fort probable que deux livres aient alors le même auteur : l'attribut «Auteur» ne serait donc plus une clé primaire. 
         
    Il peut arriver aussi que deux livres aient le même titre : l'attribut «Titre» n'est donc pas une bonne clé primaire.

    Par définition, l'attribut «ISBN» pourrait jouer le rôle de clé primaire.

    Quant à l'attribut «Code», il s'agit sans doute d'une nomenclature «maison» correspondant à une étiquette collée sur la tranche des livres : c'est donc une clé primaire qu'on qualifiera d'«artificielle».  

!!! warning "Attention" 

    Il ne peut pas y avoir deux clés primaires dans une table. La clé primaire choisie ici serait sans aucun doute l'attribut «Code».




## 4. D'autres relations

Ajoutons maintenant les relations ci-dessous :

 **Relation «Emprunts»** 

| id_emprunteur | date       | Nom    | Prénom | titre             | auteur          | code |
|---------------|------------|--------|--------|-------------------|-----------------|------|
| 845           | 12/10/2020 | DURAND | Michel | Au revoir là-haut | Pierre LEMAITRE | 942  |
| 125           | 13/10/2020 | MARTIN | Jean   | Pas pleurer       | Lydie SALVAYRE  | 1023 |
| 125           | 13/10/2020 | MARTIN | Jean   | Boussole          | Mathias ENARD   | 486  |

 **Relation «Emprunteurs»** 

| id_emprunteur | Nom    | Prénom | 
|---------------|--------|--------|
| 129           | DULAC | Marcel  | 
| 845           | DURAND | Michel |
| 125           | MARTIN | Jean   |

L'attribut «id_emprunteur» est une clé primaire de la relation «Emprunteurs».


**Notion de clé étrangère** 

Y-a-t-il une clé primaire dans la relation «Emprunts» ? 

«id_emprunteur» est bien une clé primaire (d'«Emprunteurs») mais ne peut pas être une clé primaire d'«Emprunts», car une personne peut prendre plusieurs livres à la fois : on dit que c'est une **clé étrangère**. 

!!! note "Clé étrangère"
    Une clé étrangère est une clé primaire d'une autre relation.

«code» est aussi une clé étrangère : c'est une clé primaire (de la relation «livres») mais elle ne peut pas jouer le rôle de clé primaire pour la relation emprunt, car un même livre pourra être pris à différentes dates.

Une clé primaire pourrait alors être la combinaison («date», «code»). En effet, aucun livre ne pouvant être emprunté deux fois le même jour, la connaissance de «date» et «code» suffit à identifier n'importe quel enregistrement.

## 5. Redondance des données

La relation «Emprunts» contient des informations qui sont déjà disponibles dans d'autres relations : on dit qu'elle est **redondante**, et c'est quelque chose qu'il faut éviter. À la fois pour des raisons d'espace de stockage mais aussi de cohérence : si une modification doit être faite (un emprunteur change de prénom), cette modification ne doit être faite qu'à un seul endroit de notre base de données.

Une version non-redondante de la relation «Emprunteurs» serait donc celle-ci :

**Relation «Emprunts»** 

| id_emprunteur | date       | code |
|---------------|------------|------|
| 845           | 12/10/2020 | 942  |
| 125           | 13/10/2020 | 1023 |
| 125           | 13/10/2020 | 486  |



## 6. Les 3 contraintes d'intégrité

!!! note "Contrainte de domaine" 
    Tout attribut d'un enregistrement doit respecter le domaine indiqué dans le schéma relationnel.

Attention, certains domaines sont subtils. Par exemple, si une relation possède un attribut "Code Postal", le domaine de cet attribut devra être ```String``` plutôt que ```Entier``` . Dans le cas contraire, un enregistrement possédant le code postal ```03150``` serait converti en ```3150``` (car pour les entiers, 03150 = 3150). Or le code postal ```3150``` n'existe pas.

!!! note "Contrainte de relation" 
    Tout enregistrement est unique.  
    Cette contrainte est assurée par l'existence obligatoire d'une clé primaire.

Cette clé primaire est souvent créée de manière artificielle (voir ```id_emprunteurs```  dans la table ci-dessus par exemple).

!!! note "Contrainte de référence"
    La cohérence entre les différentes tables d'une base de données est assurée par les clés étrangères : dans une table, la valeur d'un attribut qui est clé étrangère doit obligatoirement pouvoir être retrouvée dans la table dont cet attribut est clé primaire.



![](images/rel_livres.png)



!!! question "Exercice"

    Pourquoi ne peut-on pas ajouter l'enregistrement (152, "Farenheit 451", "Ray Bradburry", "Pocket", "978-2070415731") dans la table Livres ?

    ??? soluce "Solution"

        Il existe déjà un enregistrement ayant 152 comme valeur de la clé primaire.

    Pourquoi ne peut-on pas ajouter l'enregistrement (125, 13/10/2020, 511) dans la table Emprunt ?

    ??? soluce "Solution"

        Le code 511 (clé étrangère de ma table «Emprunts_v2») ne correspond à aucun enregistrement dans la table dont il est clé primaire (la table «Livres»).

    Pourquoi ne peut-on pas ajouter l'enregistrement ("301", "Farenheit 451", "Ray Bradburry", "Pocket", "978-2070415731") dans la table Livres ?

    ??? soluce "Solution"

        La clé primaire est indiqué comme étant un entier et non une chaine de caractères.

## 7. Représentation usuelles des bases de données en modèle relationnel


!!! abstract "Schéma relationnel"

    Un schéma relationnel présente l'ensemble des relations d'une base de données relationnelle.

    Dans ce schéma, il est nécessaire de présenter plusieurs informations
    
    - l'ensemble des attributs de chaque relation ;
    - les domaines de chacun de ces attributs ;
    - les clés primaires et éventuelles clés étrangères de chaque relation.

    Par convention, les clés primaires sont soulignées et les clés étrangères sont précédées du symbole #.


On prend un exemple qui décrit les professeurs et les élèves d'un lycée, ainsi que leur répartition dans les classes.

Nous pouvons établir le schéma relationnel suivant. Pour plus de simplicité, nous renommons par ailleurs les attributs de ces relations. 

- professeurs(<u>id</u> : INT, prénom : TEXT, nom : TEXT)
- classes(<u>nom</u> : TEXT, #pp1 : INT, #pp2 : INT)
- élèves(<u>id</u> : INT, prénom : TEXT, nom : TEXT, j_nais : INT, m_nais : INT, a_nais : INT, #classe : TEXT)

Ce schéma relationnel peut également être présenté sous la forme d'un diagramme.

![Diagramme relationnel](diagramme_relation.png){margin:auto;}

???+ question "Question"

    Proposer le schéma relationnel des tables Livres, Emprunteurs et Emprunt (en version 2).

    ??? soluce "Solution"

        - Livres (Titre : TEXT, Auteur : TEXT, Éditeur : TEXT, ISBN : TEXT, <u>Code</u> : INT)
        - Emprunteur (<u>id_emprunteur</u> : INT, Nom : TEXT, Prénom : TEXT)
        - Emprunt (<u>#id_emprunteur<u/> : INT, date : DATE, <u>#code<u/> : INT)