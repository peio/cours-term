# --------- PYODIDE:code --------- #

def artiste(curs, id):
    pass

# --------- PYODIDE:corr --------- #
def artiste(curs, id):
    requete = "SELECT nom FROM ARTISTES WHERE ID_artiste = ?"
    curs.execute(requete, [id])
    r = curs.fetchall()
    if len(r) == 0:
        return None
    elif len(r) == 1:
        return r[0][0]


# --------- PYODIDE:tests --------- #
assert artiste(curseur,1) == "The Beatles"
assert artiste(curseur,42) == 'Simon & Garfunkel'
assert artiste(curseur,1000) == None

# --------- PYODIDE:secrets --------- #
assert artiste(curseur,8) == "Elvis Presley"
assert artiste(curseur,12) == 'Nirvana'
assert artiste(curseur,9) == 'Miles Davis'
assert artiste(curseur,20000) == None

