def album(curs, nombre):
    requete = "SELECT Album FROM ALBUMS WHERE Classement = ?"
    curs.execute(requete, [nombre])
    r = curs.fetchall()
    if len(r) == 0:
        return None
    elif len(r) == 1:
        return r[0][0]