# --------- PYODIDE:code --------- #
import sqlite3

bdd = sqlite3.connect("albums3.db")
curseur = bdd.cursor()

# --------- PYODIDE:post --------- #
requete = "CREATE TABLE IF NOT EXISTS ARTISTES (\
	nom	TEXT,\
	ID_artiste	INTEGER,\
	PRIMARY KEY(ID_artiste)\
);"
curseur.execute(requete)

requete = "CREATE TABLE IF NOT EXISTS ALBUMS (\
    Classement	INT,\
	Année	INT,\
	Album	TEXT,\
	ID_artiste	INT,\
	Genre	TEXT,\
	Sousgenre	TEXT,\
	FOREIGN KEY(ID_artiste) REFERENCES ARTISTES(ID_artiste),\
	PRIMARY KEY(Classement)\
);"
curseur.execute(requete)


requete = "CREATE TABLE IF NOT EXISTS COLLECTIONNEURS (\
	id_collectionneur	INTEGER,\
	nom	TEXT,\
	prenom	TEXT,\
	PRIMARY KEY(id_collectionneur)\
);"
curseur.execute(requete)


requete ="INSERT INTO ARTISTES VALUES ('The Beatles',1);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Beach Boys',2);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bob Dylan',3);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Marvin Gaye',4);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('OutKast',5);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Rolling Stones',6);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Clash',7);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Elvis Presley',8);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Miles Davis',9);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Velvet Underground',10);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Jimi Hendrix Experience',11);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Nirvana',12);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bruce Springsteen',13);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Van Morrison',14);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Michael Jackson',15);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Chuck Berry',16);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Robert Johnson',17);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('John Lennon / Plastic Ono Band',18);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Stevie Wonder',19);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('James Brown',20);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Fleetwood Mac',21);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('U2',22);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Who',23);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Led Zeppelin',24);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Joni Mitchell',25);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Ramones',26);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Band',27);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('David Bowie',28);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Carole King',29);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Eagles',30);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Muddy Waters',31);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Love',32);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Sex Pistols',33);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Doors',34);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Pink Floyd',35);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Patti Smith',36);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bob Marley & The Wailers',37);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('John Coltrane',38);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Public Enemy',39);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Allman Brothers Band',40);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Little Richard',41);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Simon & Garfunkel',42);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Al Green',43);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Ray Charles',44);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Creedence Clearwater Revival',45);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Captain Beefheart & His Magic Band',46);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Sly & The Family Stone',47);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Guns N'' Roses',48);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Phil Spector',49);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Radiohead',50);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Billy Joel',51);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Paul Simon',52);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Curtis Mayfield',53);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Neil Young',54);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Prince and the Revolution',55);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('AC/DC',56);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Otis Redding',57);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('John Lennon',58);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Aretha Franklin',59);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Johnny Cash',60);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Dusty Springfield',61);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Elton John',62);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Buddy Holly',63);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Prince',64);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Hank Williams',65);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Elvis Costello',66);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Zombies',67);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Frank Sinatra',68);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Cream',69);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('James Taylor',70);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Sam Cooke',71);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Mamas and the Papas',72);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Derek and the Dominos',73);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Kanye West',74);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Etta James',75);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Byrds',76);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Various Artists',77);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Run D.M.C.',78);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Moby Grape',79);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Janis Joplin',80);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Wailers',81);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Iggy and The Stooges',82);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Talking Heads',83);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Television',84);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Black Sabbath',85);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Notorious B.I.G.',86);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Pavement',87);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Replacements',88);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Dr. Dre',89);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Meters',90);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Blondie',91);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('B.B. King',92);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Dr. John, the Night Tripper',93);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('N.W.A',94);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Steely Dan',95);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jefferson Airplane',96);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Crosby, Stills, Nash & Young',97);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Santana',98);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Arcade Fire',99);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The B 52''s',100);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('A Tribe Called Quest',101);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Howlin'' Wolf',102);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Pretenders',103);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Beastie Boys',104);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Joy Division',105);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('KISS',106);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('T. Rex',107);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Linda Ronstadt',108);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Elvis Costello & The Attractions',109);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Metallica',110);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Rod Stewart',111);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Todd Rundgren',112);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Carpenters',113);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Aerosmith',114);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Funkadelic',115);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Curtis Mayfield and The Impressions',116);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('ABBA',117);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Willie Nelson',118);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Madonna',119);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Stooges',120);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Peter Gabriel',121);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Buffalo Springfield',122);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Quicksilver Messenger Service',123);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Flying Burrito Brothers',124);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Green Day',125);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Lou Reed',126);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('John Mayall & The Bluesbreakers',127);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('R.E.M.',128);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Little Walter',129);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Strokes',130);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Nine Inch Nails',131);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Cat Stevens',132);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Pearl Jam',133);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Neil Young & Crazy Horse',134);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Ike & Tina Turner',135);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('New York Dolls',136);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bo Diddley',137);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bobby Blue Bland',138);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Smiths',139);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('My Bloody Valentine',140);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Professor Longhair',141);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Neil Diamond',142);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Pixies',143);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Eric B. & Rakim',144);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bonnie Raitt',145);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Queen',146);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Kinks',147);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Patsy Cline',148);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jackie Wilson',149);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Eminem',150);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jerry Lee Lewis',151);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Mothers of Invention',152);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Grateful Dead',153);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Ornette Coleman',154);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jay Z',155);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Kraftwerk',156);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Whitney Houston',157);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Janet',158);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Grateful Dead',159);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Crosby, Stills & Nash',160);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Tracy Chapman',161);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Blood, Sweat & Tears',162);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Jesus and Mary Chain',163);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Sleater Kinney',164);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Smokey Robinson & The Miracles',165);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('LaBelle',166);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Parliament',167);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Janet Jackson',168);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Various',169);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Mary J. Blige',170);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Barry White',171);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Cars',172);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('X',173);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bob Dylan and the Band',174);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('MC5',175);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Leonard Cohen',176);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Weezer',177);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Dolly Parton',178);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jeff Buckley',179);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Lucinda Williams',180);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Beck',181);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Red Hot Chili Peppers',182);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jane''s Addiction',183);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Lauryn Hill',184);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Tom Petty and the Heartbreakers',185);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The O''Jays',186);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Nick Drake',187);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Randy Newman',188);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Police',189);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Eric Clapton',190);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Cure',191);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Liz Phair',192);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Sonic Youth',193);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Richard & Linda Thompson',194);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Graham Parker & The Rumour',195);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Soundgarden',196);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jethro Tull',197);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Big Brother & the Holding Company',198);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Tom Waits',199);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Black Flag',200);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Moby',201);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Depeche Mode',202);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Meat Loaf',203);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('De La Soul',204);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Yardbirds',205);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Dire Straits',206);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Buzzcocks',207);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Smashing Pumpkins',208);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('New Order',209);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Rage Against the Machine',210);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Mott the Hoople',211);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Arctic Monkeys',212);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Roxy Music',213);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Jackson Browne',214);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Bjork',215);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('John Lee Hooker',216);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Oasis',217);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('TLC',218);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Toots & The Maytals',219);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Modern Lovers',220);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Wu Tang Clan',221);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Don Henley',222);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The White Stripes',223);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('M.I.A.',224);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('LCD Soundsystem',225);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Massive Attack',226);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('ZZ Top',227);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Temptations',228);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Nas',229);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Lynyrd Skynyrd',230);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Dr. John',231);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Big Star',232);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('PJ Harvey',233);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Sinead O''Connor',234);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Wire',235);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Minutemen',236);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Go Go''s',237);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Van Halen',238);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Paul McCartney & Wings',239);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Portishead',240);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Crickets',241);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Ronettes',242);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Diana Ross & The Supremes',243);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Gram Parsons',244);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Cheap Trick',245);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Peter Wolf',246);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Brian Eno',247);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Vampire Weekend',248);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('George Harrison',249);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Lil Wayne',250);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Pogues',251);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Suicide',252);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('DEVO',253);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('War',254);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Steve Miller Band',255);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Stan GetzÃŠ/ÃŠJoao GilbertoÃŠfeaturingÃŠAntonio Carlos Jobim',256);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Amy Winehouse',257);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('John Prine',258);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('EPMD',259);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Alice Cooper',260);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Los Lobos',261);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('My Morning Jacket',262);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Drifters',263);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Hole',264);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Public Image Ltd.',265);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Echo and The Bunnymen',266);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Def Leppard',267);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Magnetic Fields',268);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Coldplay',269);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Paul Butterfield Blues Band',270);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Fugees',271);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('L.L. Cool J',272);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('George Michael',273);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Manu Chao',274);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Merle Haggard',275);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Loretta Lynn',276);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Raekwon',277);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('D''Angelo',278);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Steve Earle',279);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Gang of Four',280);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Earth, Wind & Fire',281);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Cyndi Lauper',282);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Husker Du',283);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Albert King',284);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Eurythmics',285);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Wilco',286);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('MGMT',287);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('Boz Scaggs',288);"
curseur.execute(requete)
requete ="INSERT INTO ARTISTES VALUES ('The Stone Roses',289);"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (1,1967,'Sgt. Pepper''s Lonely Hearts Club Band',1,'Rock','Rock & Roll, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (2,1966,'Pet Sounds',2,'Rock','Pop Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (3,1966,'Revolver',1,'Rock','Psychedelic Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (4,1965,'Highway 61 Revisited',3,'Rock','Folk Rock, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (6,1971,'What''s Going On',4,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (5,1965,'Rubber Soul',1,'Rock, Pop','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (500,1998,'Aquemini',5,'Hip Hop','Reggae, Gangsta, Soul, Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (7,1972,'Exile on Main St.',6,'Rock','Blues Rock, Rock & Roll, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (8,1979,'London Calling',7,'Rock','Punk, New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (9,1966,'Blonde on Blonde',3,'Rock, Blues','Folk Rock, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (10,1968,'The Beatles (The White Album)',1,'Rock','Rock & Roll, Pop Rock, Psychedelic Rock, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (11,1976,'The Sun Sessions',8,'Rock','Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (12,1959,'Kind of Blue',9,'Jazz','Modal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (13,1967,'The Velvet Underground & Nico',10,'Rock','Garage Rock, Art Rock, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (14,1969,'Abbey Road',1,'Rock','Psychedelic Rock, Classic Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (15,1967,'Are You Experienced',11,'Rock, Blues','Blues Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (16,1975,'Blood on the Tracks',3,'Rock','Folk Rock, Acoustic, Ballad');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (17,1991,'Nevermind',12,'Rock','Alternative Rock, Grunge');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (18,1975,'Born to Run',13,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (19,1968,'Astral Weeks',14,'Jazz, Rock, Blues, Folk, World, & Country','Acoustic, Classic Rock, Free Improvisation');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (20,1982,'Thriller',15,'Funk / Soul, Pop','Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (21,1982,'The Great Twenty_Eight',16,'Rock','Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (22,1990,'The Complete Recordings',17,'Blues','Delta Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (23,1970,'John Lennon/Plastic Ono Band',18,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (24,1973,'Innervisions',19,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (25,1963,'Live at the Apollo, 1962',20,'Funk / Soul','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (26,1977,'Rumours',21,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (27,1987,'The Joshua Tree',22,'Rock','Alternative Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (28,1971,'Who''s Next',23,'Rock','Hard Rock, Mod, Prog Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (29,1969,'Led Zeppelin',24,'Rock','Blues Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (30,1971,'Blue',25,'Pop','Acoustic, Ballad, Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (31,1965,'Bringing It All Back Home',3,'Rock, Folk, World, & Country','Folk Rock, Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (32,1969,'Let It Bleed',6,'Rock','Blues Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (33,1976,'Ramones',26,'Rock','Rock & Roll, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (34,1968,'Music From Big Pink',27,'Rock','Folk Rock, Acoustic, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (35,1972,'The Rise and Fall of Ziggy Stardust and the Spiders From Mars',28,'Rock','Classic Rock, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (36,1971,'Tapestry',29,'Rock, Pop','Folk Rock, Soft Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (37,1976,'Hotel California',30,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (38,2001,'The Anthology',31,'Folk, World, & Country','Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (39,1963,'Please Please Me',1,'Rock','Beat, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (40,1967,'Forever Changes',32,'Rock','Folk Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (41,1977,'Never Mind the Bollocks Here''s the Sex Pistols',33,'Rock','Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (42,1967,'The Doors',34,'Rock','Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (43,1973,'The Dark Side of the Moon',35,'Rock','Prog Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (44,1975,'Horses',36,'Rock','Art Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (45,1969,'The Band (The Brown Album)',27,'Classical, Stage & Screen','Soundtrack, Modern Classical, Contemporary, Score');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (46,1984,'Legend: The Best of Bob Marley and The Wailers',37,'Reggae','Reggae, Roots Reggae');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (47,1965,'A Love Supreme',38,'Jazz','Free Jazz, Hard Bop, Modal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (48,1988,'It Takes a Nation of Millions to Hold Us Back',39,'Hip Hop','Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (49,1971,'At Fillmore East',40,'Rock, Blues','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (50,1957,'Here''s Little Richard',41,'Rock, Blues','Rock & Roll, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (51,1970,'Bridge Over Troubled Water',42,'Rock','Folk Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (52,1975,'Greatest Hits',43,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (53,1964,'Meet The Beatles!',1,'Rock','Beat, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (54,1991,'The Birth of Soul',44,'Jazz, Funk / Soul','Rhythm & Blues, Big Band, Soul, Soul-Jazz');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (55,1968,'Electric Ladyland',11,'Rock, Blues','Electric Blues, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (56,1956,'Elvis Presley',8,'Rock','Rock & Roll, Rockabilly');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (57,1976,'Songs in the Key of Life',19,'Funk / Soul','Soul, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (58,1968,'Beggars Banquet',6,'Rock, Funk / Soul, Pop','Blues Rock, Southern Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (59,1976,'Chronicle: The 20 Greatest Hits',45,'Rock','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (60,1969,'Trout Mask Replica',46,'Rock, Blues','Dialogue, Field Recording, Avantgarde, Electric Blues, Psychedelic Rock, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (61,1970,'Greatest Hits',47,'Funk / Soul','Rhythm & Blues, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (62,1987,'Appetite for Destruction',48,'Rock','Hard Rock, Heavy Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (63,1991,'Achtung Baby',22,'Electronic, Rock','Pop Rock, Synth-pop, Alternative Rock, Arena Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (64,1971,'Sticky Fingers',6,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (65,1991,'Back to Mono (1958-1969)',49,'Rock, Funk / Soul, Pop','Doo Wop, Pop Rock, Ballad, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (66,1970,'Moondance',14,'Jazz, Rock, Funk / Soul, Folk, World, & Country','Folk Rock, Rhythm & Blues, Classic Rock, Contemporary Jazz');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (67,2000,'Kid A',50,'Electronic, Rock','Alternative Rock, IDM, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (68,1979,'Off the Wall',15,'Funk / Soul, Pop','Disco, Soul, Ballad');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (69,1971,'[Led Zeppelin IV]',24,'Rock','Hard Rock, Classic Rock, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (70,1977,'The Stranger',51,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (71,1986,'Graceland',52,'Jazz, Rock, Funk / Soul, Pop, Folk, World, & Country','Folk Rock, Pop Rock, African, Afrobeat, Zydeco, Funk, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (72,1972,'Superfly',53,'Funk / Soul, Stage & Screen','Soundtrack, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (73,1975,'Physical Graffiti',24,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (74,1970,'After the Gold Rush',54,'Rock','Rock & Roll, Country Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (75,1991,'Star Time',20,'Funk / Soul','Soul, Funk, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (76,1984,'Purple Rain',55,'Electronic, Rock, Funk / Soul, Stage & Screen','Pop Rock, Funk, Soundtrack, Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (77,1980,'Back in Black',56,'Rock','Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (78,1965,'Otis Blue: Otis Redding Sings Soul',57,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (79,1969,'Led Zeppelin II',24,'Rock','Blues Rock, Classic Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (80,1971,'Imagine',58,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (81,1977,'The Clash',7,'Rock','Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (82,1972,'Harvest',54,'Rock','Folk Rock, Country Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (83,1967,'Axis: Bold as Love',11,'Rock','Psychedelic Rock, Electric Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (84,1967,'I Never Loved a Man the Way I Love You',59,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (85,1968,'Lady Soul',59,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (86,1984,'Born in the U.S.A.',13,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (87,1979,'The Wall',35,'Rock','Alternative Rock, Prog Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (88,1968,'At Folsom Prison',60,'Folk, World, & Country','Country');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (89,1969,'Dusty in Memphis',61,'Rock, Funk / Soul','Pop Rock, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (90,1972,'Talking Book',19,'Funk / Soul','Soul, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (91,1973,'Goodbye Yellow Brick Road',62,'Rock','Pop Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (92,1978,'20 Golden Greats',63,'Rock','Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (93,1987,'Sign Peace the Times',64,'Rock, Reggae','Ska, Reggae-Pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (94,1984,'40 Greatest Hits',65,'Folk, World, & Country','Country');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (95,1970,'Bitches Brew',9,'Jazz','Fusion');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (96,1969,'Tommy',23,'Rock','Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (97,1963,'The Freewheelin'' Bob Dylan',3,'Folk, World, & Country','Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (98,1978,'This Year''s Model',66,'Rock','New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (99,1971,'There''s a Riot Goin'' On',47,'Funk / Soul','Rhythm & Blues, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (100,1968,'Odessey and Oracle',67,'Rock','Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (101,1955,'In the Wee Small Hours',68,'Jazz, Pop','Big Band, Ballad');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (102,1966,'Fresh Cream',69,'Rock, Blues','Blues Rock, Electric Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (103,1959,'Giant Steps',38,'Jazz','Hard Bop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (104,1970,'Sweet Baby James',70,'Rock','Folk Rock, Acoustic, Soft Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (105,1962,'Modern Sounds in Country and Western Music',44,'Funk / Soul, Folk, World, & Country','Country, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (106,1977,'Rocket to Russia',26,'Rock','Rock & Roll, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (107,2003,'Portrait of a Legend 1951-1964',71,'Latin, Funk / Soul','Soul, Rhythm & Blues, Gospel, Cha-Cha');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (108,1971,'Hunky Dory',28,'Rock','Classic Rock, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (109,1966,'Aftermath',6,'Rock','Blues Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (110,1970,'Loaded',10,'Rock','Art Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (111,1995,'The Bends',50,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (112,1966,'If You Can Believe Your Eyes and Ears',72,'Rock','Folk Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (113,1974,'Court and Spark',25,'Rock','Soft Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (114,1967,'Disraeli Gears',69,'Rock','Psychedelic Rock, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (115,1967,'The Who Sell Out',23,'Rock','Psychedelic Rock, Mod');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (116,1965,'Out of Our Heads',6,'Rock','Blues Rock, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (117,1970,'Layla and Other Assorted Love Songs',73,'Rock','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (118,2005,'Late Registration',74,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (119,1960,'At Last!',75,'Funk / Soul, Blues','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (120,1968,'Sweetheart of the Rodeo',76,'Rock','Folk Rock, Country Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (121,1969,'Stand!',47,'Funk / Soul','Funk, Psychedelic, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (122,1972,'The Harder They Come',77,'Reggae,ÃŠPop,ÃŠFolk, World, & Country,ÃŠStage & Screen','Reggae,ÃŠRoots Reggae,ÃŠRocksteady,ÃŠContemporary,ÃŠSoundtrack');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (123,1986,'Raising Hell',78,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (124,1967,'Moby Grape',79,'Rock','Folk Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (125,1971,'Pearl',80,'Rock, Blues','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (126,1973,'Catch a Fire',81,'Reggae','Roots Reggae');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (127,1967,'Younger Than Yesterday',76,'Rock','Folk Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (128,1973,'Raw Power',82,'Rock','Garage Rock, Hard Rock, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (129,1980,'Remain in Light',83,'Electronic, Rock','New Wave, Art Rock, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (130,1977,'Marquee Moon',84,'Rock','New Wave, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (131,1970,'Paranoid',85,'Rock','Hard Rock, Heavy Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (132,1977,'Saturday Night Fever: The Original Movie Sound Track',77,'Electronic,ÃŠStage & Screen','Soundtrack,ÃŠDisco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (133,1973,'The Wild, the Innocent & the E Street Shuffle',13,'Rock','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (134,1994,'Ready to Die',86,'Hip Hop','Thug Rap');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (135,1992,'Slanted and Enchanted',87,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (136,1974,'Greatest Hits',62,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (137,1985,'Tim',88,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (138,1992,'The Chronic',89,'Hip Hop','Gangsta');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (139,1974,'Rejuvenation',90,'Funk / Soul','Bayou Funk, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (140,1978,'Parallel Lines',91,'Electronic, Rock','New Wave, Pop Rock, Punk, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (141,1965,'Live at the Regal',92,'Blues','Chicago Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (142,1963,'A Christmas Gift for You From Phil Spector',49,'Rock, Funk / Soul, Pop','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (143,1968,'Gris-Gris',93,'Jazz, Rock, Funk / Soul, Blues','Soul-Jazz, Louisiana Blues, Fusion, Bayou Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (144,1988,'Straight Outta Compton',94,'Hip Hop','Gangsta');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (145,1977,'Aja',95,'Jazz, Rock','Jazz-Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (146,1967,'Surrealistic Pillow',96,'Rock','Folk Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (147,1970,'Deja vu',97,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (148,1973,'Houses of the Holy',24,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (149,1969,'Santana',98,'Rock, Latin, Funk / Soul','Afro-Cuban, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (150,1978,'Darkness on the Edge of Town',13,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (151,2004,'Funeral',99,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (152,1979,'The B 52''s / Play Loud',100,'Electronic, Rock, Pop','New Wave, Punk, Mod');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (153,1991,'The Low End Theory',101,'Hip Hop','Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (154,1958,'Moanin'' in the Moonlight',102,'Blues','Chicago Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (155,1980,'Pretenders',103,'Rock','Alternative Rock, New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (156,1989,'Paul''s Boutique',104,'Hip Hop, Rock, Funk / Soul','Alternative Rock, Pop Rap, Psychedelic');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (157,1980,'Closer',105,'Rock','Post-Punk, New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (158,1975,'Captain Fantastic and the Brown Dirt Cowboy',62,'Rock','Pop Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (159,1975,'Alive!',106,'Rock','Hard Rock, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (160,1971,'Electric Warrior',107,'Rock','Glam, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (161,1968,'The Dock of the Bay',57,'Funk / Soul','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (162,1997,'OK Computer',50,'Electronic, Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (163,1982,'1999',64,'Funk / Soul, Pop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (164,2002,'The Very Best of Linda Ronstadt',108,'Rock, Pop','Soft Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (165,1973,'Let''s Get It On',4,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (166,1982,'Imperial Bedroom',109,'Rock','Alternative Rock, New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (167,1986,'Master of Puppets',110,'Rock','Thrash, Speed Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (168,1977,'My Aim Is True',66,'Rock','New Wave, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (169,1977,'Exodus',37,'Reggae','Reggae, Roots Reggae');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (170,1970,'Live at Leeds',23,'Rock','Classic Rock, Blues Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (171,1968,'The Notorious Byrd Brothers',76,'Rock','Psychedelic Rock, Folk Rock, Country Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (172,1971,'Every Picture Tells a Story',111,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (173,1972,'Something/Anything?',112,'Rock','Power Pop, Pop Rock, Prog Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (174,1976,'Desire',3,'Rock','Folk Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (175,1970,'Close to You',113,'Rock, Pop','Pop Rock, Vocal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (176,1976,'Rocks',114,'Rock','Hard Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (177,1978,'One Nation Under a Groove',115,'Funk / Soul','P.Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (178,1992,'The Anthology: 1961-1977',116,'Funk / Soul','Rhythm & Blues, Soul, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (179,2001,'The Definitive Collection',117,'Electronic, Pop','Europop, Synth-pop, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (180,1965,'The Rolling Stones, Now!',6,'Rock, Blues, Pop','Pop Rock, Rhythm & Blues, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (181,1974,'Natty Dread',37,'Reggae','Reggae, Roots Reggae');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (182,1975,'Fleetwood Mac',21,'Rock, Pop','Folk Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (183,1975,'Red Headed Stranger',118,'Folk, World, & Country','Country');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (184,1990,'The Immaculate Collection',119,'Electronic, Pop','Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (185,1969,'The Stooges',120,'Rock','Garage Rock, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (186,1973,'Fresh',47,'Funk / Soul','Rhythm & Blues, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (187,1986,'So',121,'Electronic, Rock, Funk / Soul, Pop','Art Rock, Pop Rock, Synth-pop, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (188,1967,'Buffalo Springfield Again',122,'Rock, Pop','Country Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (189,1969,'Happy Trails',123,'Rock','Acid Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (190,1969,'From Elvis in Memphis',8,'Rock, Funk / Soul, Folk, World, & Country','Country, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (191,1970,'Fun House',120,'Rock','Garage Rock, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (192,1969,'The Gilded Palace of Sin',124,'Rock, Folk, World, & Country','Country Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (193,1994,'Dookie',125,'Rock','Alternative Rock, Pop Punk, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (194,1972,'Transformer',126,'Rock','Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (195,1966,'Blues Breakers With Eric Clapton (The Beano Album)',127,'Rock,ÃŠBlues','Blues Rock,ÃŠElectric Blues,ÃŠHarmonica Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (196,1998,'Nuggets: Original Artyfacts From the First Psychedelic Era, 1965-1968',77,'Rock','Garage Rock,ÃŠPsychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (197,1983,'Murmur',128,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (198,1967,'The Best of Little Walter',129,'Blues','Chicago Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (199,2001,'Is This It',130,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (200,1979,'Highway to Hell',56,'Rock','Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (201,1994,'The Downward Spiral',131,'Electronic, Rock','Industrial');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (202,1966,'Parsley, Sage, Rosemary and Thyme',42,'Rock, Pop, Folk, World, & Country','Folk Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (203,1987,'Bad',15,'Funk / Soul, Pop','Pop Rock, Ballad, Funk, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (204,2006,'Modern Times',3,'Rock','Blues Rock, Folk Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (205,1968,'Wheels of Fire',69,'Rock, Blues','Blues Rock, Psychedelic Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (206,1980,'Dirty Mind',64,'Funk / Soul','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (207,1970,'Abraxas',98,'Rock, Latin','Fusion, Hard Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (208,1970,'Tea for the Tillerman',132,'Rock, Stage & Screen','Folk Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (209,1991,'Ten',133,'Rock','Grunge');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (210,1969,'Everybody Knows This Is Nowhere',134,'Rock','Folk Rock, Country Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (211,1975,'Wish You Were Here',35,'Rock','Classic Rock, Prog Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (212,1994,'Crooked Rain Crooked Rain',87,'Rock','Alternative Rock, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (213,1981,'Tattoo You',6,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (214,1991,'Proud Mary: The Best of Ike and Tina Turner',135,'Funk / Soul','Rhythm & Blues, Bayou Funk, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (215,1973,'New York Dolls',136,'Rock','Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (216,1986,'Bo Diddley / Go Bo Diddley',137,'Rock, Blues','Rhythm & Blues, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (217,1961,'Two Steps From the Blues',138,'Funk / Soul, Blues','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (218,1986,'The Queen Is Dead',139,'Rock, Pop','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (219,1986,'Licensed to Ill',104,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (220,1970,'Look-Ka Py Py',90,'Funk / Soul','Bayou Funk, Soul, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (221,1991,'Loveless',140,'Rock','Alternative Rock, Shoegaze');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (222,1972,'New Orleans Piano',141,'Funk / Soul, Blues','Piano Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (223,1983,'War',22,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (224,1999,'The Neil Diamond Collection',142,'Rock, Pop','Soft Rock, Ballad');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (225,2004,'American Idiot',125,'Rock','Pop Rock, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (226,1982,'Nebraska',13,'Rock','Acoustic');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (227,1989,'Doolittle',143,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (228,1987,'Paid in Full',144,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (229,1975,'Toys in the Attic',114,'Rock','Hard Rock, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (230,1989,'Nick of Time',145,'Rock, Pop','Blues Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (231,1975,'A Night at the Opera',146,'Rock','Hard Rock, Pop Rock, Prog Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (232,1972,'The Kink Kronikles',147,'Rock','Rock & Roll, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (233,1965,'Mr. Tambourine Man',76,'Rock','Folk Rock, Garage Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (234,1968,'Bookends',42,'Rock','Folk Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (235,2000,'The Ultimate Collection',148,'Folk, World, & Country','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (236,1992,'Mr. Excitement!',149,'Funk / Soul','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (237,1965,'My Generation',23,'Rock','Mod');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (238,1962,'Howlin'' Wolf',102,'Blues','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (239,1989,'Like a Prayer',119,'Electronic, Pop','Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (240,1972,'Can''t Buy a Thrill',95,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (241,1984,'Let It Be',88,'Rock','Alternative Rock, Power Pop, Punk, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (242,1984,'Run D.M.C.',78,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (243,1970,'Black Sabbath',85,'Rock','Blues Rock, Heavy Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (244,2000,'The Marshall Mathers LP',150,'Hip Hop','Pop Rap, Hardcore Hip-Hop, Horrorcore');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (245,1993,'All Killer No Filler! The Jerry Lee Lewis Anthology',151,'Rock, Blues, Folk, World, & Country','Country Blues, Rock & Roll, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (246,1966,'Freak Out!',152,'Electronic, Rock','Musique Concr?te, Avantgarde, Symphonic Rock, Rhythm & Blues, Psychedelic Rock, Experimental, Parody');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (247,1969,'Live/Dead',153,'Rock','Folk Rock, Country Rock, Psychedelic Rock, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (248,1959,'The Shape of Jazz to Come',154,'Jazz','Free Jazz');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (249,1992,'Automatic for the People',128,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (250,1996,'Reasonable Doubt',155,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (251,1977,'Low',28,'Electronic, Rock','Art Rock, Ambient, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (252,2001,'The Blueprint',155,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (253,1980,'The River',13,'Rock','Folk Rock, Pop Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (254,1966,'Complete & Unbelievable: The Otis Redding Dictionary of Soul',57,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (255,1991,'Metallica (The Black Album)',110,'Rock','Heavy Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (256,1977,'Trans Europa Express',156,'Electronic','Electro');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (257,1985,'Whitney Houston',157,'Funk / Soul, Pop','Synth-pop, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (258,1968,'The Kinks Are The Village Green Preservation Society',147,'Rock','Psychedelic Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (259,1997,'The Velvet Rope',158,'Electronic, Funk / Soul, Pop','RnB/Swing, Downtempo');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (260,1978,'Stardust',118,'Pop, Folk, World, & Country','Vocal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (261,1970,'American Beauty',159,'Rock','Folk Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (262,1969,'Crosby, Stills & Nash',160,'Rock, Folk, World, & Country','Folk Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (263,1988,'Tracy Chapman',161,'Rock','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (264,1970,'Workingman''s Dead',159,'Rock','Folk Rock, Country Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (265,1959,'The Genius of Ray Charles',44,'Jazz, Pop','Soul-Jazz, Big Band');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (266,1968,'Child Is Father to the Man',162,'Rock','Blues Rock, Jazz-Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (267,1973,'Quadrophenia',23,'Rock','Hard Rock, Classic Rock, Mod');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (268,1972,'Paul Simon',52,'Rock','Folk Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (269,1985,'Psychocandy',163,'Rock','Noise');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (270,1978,'Some Girls',6,'Rock','Blues Rock, Rock & Roll, Classic Rock, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (271,1965,'The Beach Boys Today!',2,'Rock','Pop Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (272,1997,'Dig Me Out',164,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (273,1966,'Going to a Go-Go',165,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (274,1974,'Nightbirds',166,'Funk / Soul','Funk, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (275,1999,'The Slim Shady LP',150,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (276,1975,'Mothership Connection',167,'Funk / Soul','P.Funk, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (277,1989,'Rhythm Nation 1814',168,'Electronic, Hip Hop, Pop','RnB/Swing, Downtempo, Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (278,1997,'Anthology of American Folk Music',169,'Blues, Folk, World, & Country','Cajun, Country, Field Recording, Gospel, Delta Blues, Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (279,1973,'Aladdin Sane',28,'Rock','Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (280,2000,'All That You Can''t Leave Behind',22,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (281,1994,'My Life',170,'Hip Hop','RnB/Swing');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (282,1964,'Folk Singer',31,'Blues','Delta Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (283,1974,'Can''t Get Enough',171,'Funk / Soul','Soul, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (284,1978,'The Cars',172,'Electronic, Rock','New Wave, Pop Rock, Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (285,1972,'Music of My Mind',19,'Funk / Soul','Soul-Jazz, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (286,1972,'I''m Still in Love With You',43,'Funk / Soul','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (287,1980,'Los Angeles',173,'Rock','Punk, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (288,1968,'Anthem of the Sun',159,'Rock','Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (289,1967,'Something Else by The Kinks',147,'Rock','Pop Rock, Psychedelic Rock, Mod');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (290,1973,'Call Me',43,'Funk / Soul','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (291,1977,'Talking Heads: 77',83,'Rock','New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (292,1975,'The Basement Tapes',174,'Rock','Folk Rock, Country Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (293,1968,'White Light/White Heat',10,'Rock','Avantgarde, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (294,1969,'Kick Out the Jams',175,'Rock','Garage Rock, Hard Rock, Punk, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (295,1970,'Songs of Love and Hate',176,'Folk, World, & Country','Folk Rock, Folk, Ballad');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (296,1985,'Meat Is Murder',139,'Rock','Alternative Rock, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (297,1968,'We''re Only in It for the Money',152,'Electronic, Rock','Modern Classical, Avantgarde, Psychedelic Rock, Experimental, Parody');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (298,2003,'The College Dropout',74,'Hip Hop','Pop Rap, Conscious, Contemporary R&B');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (299,1994,'Weezer (Blue Album)',177,'Rock','Alternative Rock, Emo');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (300,1971,'Master of Reality',85,'Rock','Hard Rock, Heavy Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (301,1971,'Coat of Many Colors',178,'Folk, World, & Country','Country');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (302,1990,'Fear of a Black Planet',39,'Hip Hop','Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (303,1967,'John Wesley Harding',3,'Rock','Folk Rock, Country Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (304,1994,'Grace',179,'Rock','Alternative Rock, Folk Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (305,1998,'Car Wheels on a Gravel Road',180,'Folk, World, & Country','Country, Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (306,1996,'Odelay',181,'Electronic, Hip Hop, Funk / Soul, Pop','Electro, Downtempo, Hip Hop, Disco, Afrobeat, Abstract');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (307,1964,'A Hard Day''s Night',1,'Rock, Stage & Screen','Soundtrack, Beat, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (308,1956,'Songs for Swingin'' Lovers!',68,'Jazz, Pop','Vocal, Easy Listening');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (309,1969,'Willy and the Poor Boys',45,'Rock','Blues Rock, Rock & Roll, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (310,1991,'Blood Sugar Sex Magik',182,'Rock','Alternative Rock, Funk Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (311,1994,'The Sun Records Collection',169,'Rock, Funk / Soul, Blues, Pop, Folk, World, & Country','Country Blues, Rock & Roll, Rockabilly');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (312,1988,'Nothing''s Shocking',183,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (313,1994,'MTV Unplugged in New York',12,'Rock','Folk Rock, Acoustic, Grunge');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (314,1998,'The Miseducation of Lauryn Hill',184,'Hip Hop','RnB/Swing, Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (315,1979,'Damn the Torpedoes',185,'Rock','Soft Rock, Hard Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (316,1969,'The Velvet Underground',10,'Rock','Garage Rock, Art Rock, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (317,1988,'Surfer Rosa',143,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (318,1972,'Back Stabbers',186,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (319,1973,'Burnin''',81,'Reggae','Reggae');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (320,2001,'Amnesiac',50,'Electronic, Rock','Alternative Rock, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (321,1972,'Pink Moon',187,'Rock, Folk, World, & Country','Folk, Folk Rock, Acoustic');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (322,1972,'Sail Away',188,'Rock, Pop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (323,1981,'Ghost in the Machine',189,'Rock, Pop','Alternative Rock, New Wave, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (324,1976,'Station to Station',28,'Rock, Funk / Soul','Classic Rock, Soul, Funk, Art Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (325,1977,'Slowhand',190,'Rock','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (326,1989,'Disintegration',191,'Electronic, Rock','New Wave, Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (327,1993,'Exile in Guyville',192,'Rock','Lo-Fi, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (328,1988,'Daydream Nation',193,'Rock','Alternative Rock, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (329,1986,'In the Jungle Groove',20,'Funk / Soul','Soul, Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (330,1975,'Tonight''s the Night',54,'Rock','Blues Rock, Folk Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (331,1965,'Help!',1,'Rock, Stage & Screen','Beat, Soundtrack, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (332,1982,'Shoot Out the Lights',194,'Rock, Folk, World, & Country','Folk Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (333,1981,'Wild Gift',173,'Rock','Rock & Roll, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (334,1979,'Squeezing Out Sparks',195,'Rock, Blues','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (335,1994,'Superunknown',196,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (336,2007,'In Rainbows',50,'Electronic, Rock','Alternative Rock, Art Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (337,1971,'Aqualung',197,'Rock','Classic Rock, Prog Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (338,1968,'Cheap Thrills',198,'Rock','Folk Rock, Blues Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (339,1974,'The Heart of Saturday Night',199,'Jazz, Pop, Folk, World, & Country','Contemporary Jazz, Spoken Word');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (340,1981,'Damaged',200,'Rock','Hardcore, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (341,1999,'Play',201,'Electronic','Breakbeat, Leftfield, Downtempo');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (342,1990,'Violator',202,'Electronic','Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (343,1977,'Bat Out of Hell',203,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (344,1973,'Berlin',126,'Rock','Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (345,1984,'Stop Making Sense',83,'Rock, Funk / Soul','Funk, Indie Rock, New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (346,1989,'3 Feet High and Rising',204,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (347,1967,'The Piper at the Gates of Dawn',35,'Rock','Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (348,1960,'Muddy Waters at Newport 1960',31,'Rock, Blues','Blues Rock, Chicago Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (349,2003,'The Black Album',155,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (350,1966,'Roger the Engineer',205,'Rock, Blues','Blues Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (351,1979,'Rust Never Sleeps',134,'Rock','Hard Rock, Arena Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (352,1985,'Brothers in Arms',206,'Rock','AOR, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (353,2010,'My Beautiful Dark Twisted Fantasy',74,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (354,1978,'52nd Street',51,'Jazz, Rock, Pop','Pop Rock, Ballad, Latin Jazz');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (355,1965,'Having a Rave Up',205,'Rock','Blues Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (356,1970,'12 Songs',188,'Rock, Pop','Pop Rock, Vocal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (357,1967,'Between the Buttons',6,'Rock','Blues Rock, Psychedelic Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (358,1960,'Sketches of Spain',9,'Jazz','Modal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (359,1972,'Honky Châ€°teau',62,'Rock','Pop Rock,ÃŠClassic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (360,1979,'Singles Going Steady',207,'Rock','Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (361,2000,'Stankonia',5,'Hip Hop, Funk / Soul','Gangsta, P.Funk, Crunk, Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (362,1993,'Siamese Dream',208,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (363,1987,'Substance 1987',209,'Electronic','Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (364,1971,'L.A. Woman',34,'Rock','Blues Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (365,1992,'Rage Against the Machine',210,'Hip Hop, Rock','Funk Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (366,1994,'American Recordings',60,'Folk, World, & Country','Country, Gospel, Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (367,1998,'Ray of Light',119,'Electronic, Pop','House, Techno, Downtempo, Vocal, Ambient');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (368,1972,'Eagles',30,'Rock','Country Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (369,1987,'Louder Than Bombs',139,'Rock','Alternative Rock, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (370,1973,'Mott',211,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (371,2006,'Whatever People Say I Am, That''s What I''m Not',212,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (372,1979,'Reggatta de Blanc',189,'Rock, Pop','New Wave, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (373,1969,'Volunteers',96,'Rock','Psychedelic Rock, Folk Rock, Country Rock, Honky Tonk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (374,1975,'Siren',213,'Rock','Art Rock, Pop Rock, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (375,1974,'Late for the Sky',214,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (376,1995,'Post',215,'Electronic','Breakbeat, IDM, Electro');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (377,1991,'The Ultimate Collection: 1948-1990',216,'Blues','Country Blues, Electric Blues, Chicago Blues, Jump Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (378,1995,'(What''s the Story) Morning Glory?',217,'Rock, Pop','Brit Pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (379,1994,'CrazySexyCool',218,'Electronic, Hip Hop, Funk / Soul','RnB/Swing');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (380,1973,'Funky Kingston',219,'Reggae','Reggae Gospel, Reggae, Roots Reggae, Rocksteady');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (381,2011,'The Smile Sessions',2,'Rock','Pop Rock, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (382,1976,'The Modern Lovers',220,'Rock','Art Rock, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (383,1978,'More Songs About Buildings and Food',83,'Rock','New Wave, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (384,1966,'A Quick One',23,'Rock','Mod, Beat, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (385,2001,'Love and Theft',3,'Rock','Folk Rock, Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (386,1974,'Pretzel Logic',95,'Jazz, Rock','Jazz-Rock, Pop Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (387,1993,'Enter the Wu_Tang: 36 Chambers',221,'Hip Hop','Gangsta');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (388,1985,'The Indestructible Beat of Soweto',77,'Funk / Soul,ÃŠFolk, World, & Country','African');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (389,1989,'The End of the Innocence',222,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (390,2003,'Elephant',223,'Rock','Blues Rock, Garage Rock, Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (391,1976,'The Pretender',214,'Rock','Soft Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (392,1970,'Let It Be',1,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (393,2007,'Kala',224,'Electronic, Hip Hop, Reggae, Pop','Grime, Bollywood, Hip Hop, Dancehall');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (394,1974,'Good Old Boys',188,'Rock, Pop','Pop Rock, Vocal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (395,2007,'Sound of Silver',225,'Electronic, Rock','Leftfield, Alternative Rock, Electro, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (396,1973,'For Your Pleasure',213,'Rock','Art Rock, Avantgarde, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (397,1991,'Blue Lines',226,'Electronic, Reggae','Dub, Downtempo');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (398,1983,'Eliminator',227,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (399,1985,'Rain Dogs',199,'Rock, Blues','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (400,1995,'Anthology: The Best of The Temptations',228,'Electronic, Funk / Soul','Soul, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (401,1999,'Californication',182,'Rock','Alternative Rock, Funk Metal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (402,1994,'Illmatic',229,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (403,1973,'(pronounced ''leh-''nerd ''skin-''nerd)',230,'Rock','Blues Rock, Hard Rock, Southern Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (404,1972,'Dr. John''s Gumbo',231,'Funk / Soul','Bayou Funk, Funk, Rhythm & Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (405,1974,'Radio City',232,'Rock','Power Pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (406,1993,'Rid of Me',233,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (407,1980,'Sandinista!',7,'Rock, Reggae','Rock & Roll, Dub, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (408,1989,'I Do Not Want What I Haven''t Got',234,'Rock, Funk / Soul, Blues','Blues Rock, Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (409,1967,'Strange Days',34,'Rock','Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (410,1997,'Time Out of Mind',3,'Rock, Blues','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (411,1974,'461 Ocean Boulevard',190,'Rock','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (412,1977,'Pink Flag',235,'Rock','Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (413,1984,'Double Nickels on the Dime',236,'Rock','Alternative Rock, Hardcore, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (414,1981,'Beauty and the Beat',237,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (415,1978,'Van Halen',238,'Rock','Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (416,1999,'Mule Variations',199,'Electronic, Rock','Abstract, Art Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (417,1980,'Boy',22,'Rock','New Wave, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (418,1973,'Band on the Run',239,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (419,1994,'Dummy',240,'Electronic','Trip Hop, Downtempo');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (420,1957,'The Chirping Crickets',241,'Rock, Pop','Rockabilly, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (421,1990,'The Best of the Girl Groups, Volume 1',77,'Rock,ÃŠPop','Pop Rock, Vocal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (422,1963,'Presenting the Fabulous Ronettes Featuring Veronica',242,'Rock, Pop','Rock & Roll, Vocal');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (423,2001,'Anthology',243,'Electronic, Funk / Soul','Pop Rock, Soul, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (424,2002,'The Rising',13,'Rock','Folk Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (425,1974,'Grievous Angel',244,'Rock, Folk, World, & Country','Country Rock, Honky Tonk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (426,1978,'Cheap Trick at Budokan',245,'Rock','Power Pop, Pop Rock, Arena Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (427,2002,'Sleepless',246,'Rock, Blues, Pop','Blues Rock, Pop Rock, Ballad');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (428,1978,'Outlandos d''Amour',189,'Rock','Alternative Rock, New Wave, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (429,1975,'Another Green World',247,'Electronic','Experimental, Ambient');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (430,2007,'Vampire Weekend',248,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (431,2000,'Stories From the City, Stories From the Sea',233,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (432,1973,'Here Come the Warm Jets',247,'Rock','Art Rock, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (433,1970,'All Things Must Pass',249,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (434,1972,'#1 Record',232,'Rock','Power Pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (435,1993,'In Utero',12,'Rock','Grunge, Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (436,2002,'Sea Change',181,'Rock','Alternative Rock, Post Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (437,2008,'Tha Carter III',250,'Hip Hop, Funk / Soul','RnB/Swing, Screw, Pop Rap, Thug Rap');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (438,1979,'Boys Don''t Cry',191,'Rock','New Wave, Post-Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (439,1985,'Live at the Harlem Square Club, 1963',71,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (440,1985,'Rum Sodomy & the Lash',251,'Rock, Folk, World, & Country','Folk Rock, Celtic, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (441,1977,'Suicide',252,'Electronic, Rock','New Wave, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (442,1978,'Q: Are We Not Men? A: We Are Devo!',253,'Rock','New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (443,1977,'In Color',245,'Rock','Power Pop, Pop Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (444,1972,'The World Is a Ghetto',254,'Funk / Soul','Funk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (445,1976,'Fly Like an Eagle',255,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (446,1970,'Back in the USA',175,'Rock','Garage Rock, Rock & Roll');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (447,1964,'Getz / Gilberto',256,'Jazz','Bossa Nova,ÃŠLatin Jazz');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (448,1983,'Synchronicity',189,'Rock, Pop','New Wave, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (449,1978,'Third/Sister Lovers',232,'Rock','Lo-Fi,ÃŠIndie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (450,1973,'For Everyman',214,'Rock','Pop Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (451,2006,'Back to Black',257,'Funk / Soul, Pop','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (452,1971,'John Prine',258,'Folk, World, & Country','Country, Folk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (453,1987,'Strictly Business',259,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (454,1971,'Love It to Death',260,'Rock','Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (455,1984,'How Will the Wolf Survive?',261,'Rock, Latin','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (456,1978,'Here, My Dear',4,'Funk / Soul','Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (457,2005,'Z',262,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (458,1970,'Tumbleweed Connection',62,'Rock, Folk, World, & Country','Soft Rock, Country Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (459,1968,'The Drifters'' Golden Hits',263,'Rock, Funk / Soul','Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (460,1994,'Live Through This',264,'Rock','Grunge');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (461,1979,'Metal Box',265,'Electronic, Rock','Post-Punk, Dub, Avantgarde, Experimental');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (462,1987,'Document',128,'Rock','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (463,1981,'Heaven Up Here',266,'Rock','New Wave, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (464,1987,'Hysteria',267,'Rock','Hard Rock, Arena Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (465,1999,'69 Love Songs',268,'Electronic, Rock','Synth-pop, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (466,2002,'A Rush of Blood to the Head',269,'Rock','Alternative Rock, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (467,1987,'Tunnel of Love',13,'Rock','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (468,1965,'The Paul Butterfield Blues Band',270,'Rock, Blues','Blues Rock, Electric Blues, Chicago Blues, Modern Electric Blues, Harmonica Blues');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (469,1996,'The Score',271,'Hip Hop','Pop Rap, Conscious');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (470,1985,'Radio',272,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (471,1974,'I Want to See the Bright Lights Tonight',194,'Rock','Classic Rock, Folk Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (472,1987,'Faith',273,'Electronic, Rock, Funk / Soul, Blues, Pop','Downtempo, Soft Rock, Pop Rock, Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (473,1984,'The Smiths',139,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (474,2001,'Proxima estacion: Esperanza',274,'Rock, Reggae, Latin','Folk Rock, Reggae, Reggae-Pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (475,1979,'Armed Forces',109,'Rock','New Wave, Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (476,1997,'Life After Death',86,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (477,1996,'Down Every Road',275,'Folk, World, & Country','Country');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (478,2002,'All Time Greatest Hits',276,'Folk, World, & Country','Country');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (479,1971,'Maggot Brain',115,'Rock, Funk / Soul','P.Funk, Psychedelic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (480,1995,'Only Built 4 Cuban Linx',277,'Hip Hop','None');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (481,2000,'Voodoo',278,'Hip Hop, Funk / Soul','Soul, Funk, Neo Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (482,1986,'Guitar Town',279,'Rock, Folk, World, & Country','Country, Honky Tonk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (483,1979,'Entertainment!',280,'Rock','Post-Punk, New Wave');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (484,1972,'All the Young Dudes',211,'Rock','Classic Rock, Glam');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (485,1994,'Vitalogy',133,'Rock','Alternative Rock, Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (486,1975,'That''s the Way of the World',281,'Funk / Soul','Soul, Funk, Disco');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (487,1983,'She''s So Unusual',282,'Electronic, Rock','Pop Rock, Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (488,1985,'New Day Rising',283,'Rock','Alternative Rock, Hardcore, Punk');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (489,1976,'Destroyer',106,'Rock','Hard Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (490,1973,'Tres hombres',227,'Rock','Blues Rock, Classic Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (491,1967,'Born Under a Bad Sign',284,'Funk / Soul, Blues','Electric Blues, Rhythm & Blues, Soul');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (492,1983,'Touch',285,'Electronic, Pop','New Wave, Synth-pop');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (493,2002,'Yankee Hotel Foxtrot',286,'Rock','Alternative Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (494,2007,'Oracular Spectacular',287,'Electronic, Rock, Pop','Synth-pop, Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (495,1972,'Give It Up',145,'Rock','Blues Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (496,1969,'Boz Scaggs',288,'Rock','Pop Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (497,2001,'White Blood Cells',223,'Rock','Indie Rock, Alternative Rock, Blues Rock, Garage Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (498,1989,'The Stone Roses',289,'Rock','Indie Rock');"
curseur.execute(requete)
requete ="INSERT INTO ALBUMS VALUES (499,1971,'Live in Cook County Jail',92,'Blues','Electric Blues');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (1,'WEBER','Didier');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (2,'PERROT','AndrÃ©');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (3,'BOURGEOIS','Jean-Claude');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (4,'BERTRAND','Romain');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (5,'NICOLAS','Nicolas');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (6,'BARRE','Alice');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (7,'ANTOINE','Léa');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (8,'THOMAS','Guy');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (9,'MALLET','Dominique');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (10,'DUMONT','Patrice');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (11,'WEBER','GÃ©rard');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (12,'PICARD','Jean-Luc');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (13,'DUPUY','Raphaël');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (14,'MICHAUD','Françoise');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (15,'CARON','Bruno');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (16,'COLLET','Marc');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (17,'RENAUD','Corinne');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (18,'BRUN','Adrien');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (19,'HENRY','Brigitte');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (20,'BERGER','Patricia');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (21,'MEYER','Franck');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (22,'RODRIGUEZ','Romain');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (23,'JULIEN','Sophie');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (24,'BERGER','AnaÃ¯s');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (25,'FABRE','Patrick');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (26,'MOREL','Fernand');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (27,'GAY','Alexandre');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (28,'PELLETIER','Colette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (29,'MATHIEU','Philippe');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (30,'MICHEL','Paulette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (31,'MALLET','Claude');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (32,'DUPONT','Florence');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (33,'POIRIER','Gilles');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (34,'MICHEL','Jean-Pierre');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (35,'MALLET','Elise');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (36,'JACOB','Emmanuel');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (37,'HUMBERT','Josiane');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (38,'HUBERT','Charles');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (39,'COLIN','Olivier');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (40,'MARTIN','Roland');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (41,'VIDAL','Nicolas');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (42,'MOULIN','Suzanne');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (43,'HERVE','LÃ©a');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (44,'LEMAITRE','Odette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (45,'NOEL','Daniel');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (46,'ROBIN','Xavier');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (47,'MONNIER','Raymond');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (48,'GUICHARD','Anthony');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (49,'GUERIN','Eric');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (50,'DURAND','Madeleine');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (51,'FABRE','FranÃ§oise');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (52,'COLLIN','Daniel');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (53,'GERMAIN','HÃ©lÃ¨ne');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (54,'ANDRE','Lucie');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (55,'MEYER','Jean-Luc');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (56,'COLIN','AnaÃ¯s');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (57,'PICHON','Jeanne');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (58,'BOYER','Henri');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (59,'HERVE','Laurent');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (60,'BOURGEOIS','Sarah');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (61,'PICARD','Annie');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (62,'PICHON','Gabriel');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (63,'NICOLAS','Denis');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (64,'COLLIN','Yvette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (65,'MONNIER','Colette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (66,'MOULIN','Quentin');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (67,'PERRET','Georges');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (68,'MARECHAL','Mathilde');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (69,'DELAUNAY','Philippe');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (70,'ROUSSEAU','Karine');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (71,'MEYER','Sylvie');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (72,'GUERIN','Dominique');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (73,'LEMAIRE','Charles');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (74,'LEFEBVRE','Marion');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (75,'GRONDIN','Emilie');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (76,'VIDAL','Martine');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (77,'ROLLAND','Didier');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (78,'MEYER','Serge');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (79,'VINCENT','Yvonne');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (80,'COLLET','Alain');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (81,'CHEVALLIER','Simone');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (82,'SCHNEIDER','Gilbert');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (83,'PERROT','Sandrine');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (84,'LEGER','Bernadette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (85,'BAILLY','Pascal');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (86,'PARIS','Patrice');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (87,'BOULANGER','Jacqueline');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (88,'MALLET','Patricia');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (89,'MAILLARD','Jacqueline');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (90,'NOEL','Bernadette');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (91,'HUMBERT','VÃ©ronique');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (92,'DENIS','Raymonde');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (93,'DESCHAMPS','GeneviÃ¨ve');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (94,'MICHEL','Patricia');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (95,'DUMAS','Julien');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (96,'NOEL','AndrÃ©e');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (97,'THOMAS','Dominique');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (98,'BARTHELEMY','Louise');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (99,'LEBRUN','Jean');"
curseur.execute(requete)
requete ="INSERT INTO COLLECTIONNEURS VALUES (100,'BERTIN','Mathilde');"
curseur.execute(requete)
requete ="COMMIT;"
curseur.execute(requete)